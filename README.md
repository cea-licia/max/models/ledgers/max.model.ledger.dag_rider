# max.model.ledger.dag_rider

This model implements the DagRider algorithm, as specified in the 
"[All you need is DAG](https://dl.acm.org/doi/10.1145/3465084.3467905)" paper.

It implements at the same time the [max.model.ledger.abstract_ledger](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.abstract_ledger)
interface i.e. it implements the main service rendered by a distributed ledger which is that of ordering transactions.

## Principle of DAG-based ledgers (in a few words)

Both blockchains and DAG-based ledgers are a means to implement a Distributed Ledger.

With blockchains, the nodes that participate in the ledger regularly agree on a block of transactions that is to be added in batch
to the ledger.
The manner with which the next block is selected may vary but, in any case, blocks are added sequentially and the addition of a block
requires the agreement of a majority of nodes.

DAG-based ledgers rely on a Directed Acyclic Graph instead of on a chain of blocks.
Each node can submit its own blocks which are called vertices.
Each node can do so relatively independently of the others 
(there are still some requirements related to the number of outgoing edges but it is far less constrained than in blockchains).
The set of all submitted vertices form the Directed Acyclic Graph.
Transactions are then ordered depending on the content and the structure of the DAG.

The images below represent the same total ordering of transactions (from t1 to t5) that is equivalently obtained via:
- a blockchain
- and a DAG

| blockchains                                           | DAG-based ledgers                             |
|-------------------------------------------------------|-----------------------------------------------|
| <img src="./README_images/principle_blockchain.png">  | <img src="./README_images/principle_dag.png"> |



## Principle of the DagRider algorithm (in a few words)

Instead of presenting its pseudo-code (as in "[All you need is DAG](https://dl.acm.org/doi/10.1145/3465084.3467905)"),
let us describe it using the following diagram:

<img src="./README_images/dagrider_layers.png">

DAG-Rider is a protocol that involves 5 distinct layers:
- the application layer (in yellow on the diagram) is the client-side layer that uses the distributed ledger service implemented by DagRider:
  - Clients can send transactions in order for them to later be included in the ledger. From the perspective of the DagRider node,
  these transactions are received, which correspond to the "receive transaction" action in the diagram.
  Once received by a DagRider node (from a client), a transaction is temporary stored in the node's local mempool 
  (called "transactions mempool" in the diagram).
  - Transactions can be delivered in batch which has an effect on the application layer 
  (that of the corresponding delivery operations for all delivered transactions). This corresponds to the "deliver transaction"
  action in the diagram.
- the DagRider comm layer (in light green on the diagram) which is tasked with:
  - the creation of the vertices submitted by the node
  - the submission of said vertices
  - the reception of vertices submitted by other nodes of the network
  - the integration of all these submitted vertices into the local copy of the DAG that exists locally in the node's memory
- the reliable broadcast layer (in blue on the diagram) which is tasked with broadcasting submitted vertices reliably.
  - Whenever a node wants to propose a vertex ("propose vertex" action in the diagram), it extracts some transactions from
  the node's local mempool and creates a vertex which it sends to the reliable broadcast layer (via the "send vertex" action).
  In the reliable broadcast layer the "rbcast" action corresponds to the triggering of the process by which the vertex is reliably broadcast
  (what this specifically entails depends on the particular Byzantine Reliable Broadcast algorithm that is used).
  - Whenever a node receives a new vertex proposal, which corresponds to the delivery operation of the reliable broadcast layer 
  (i.e. the finalization of the process by which the vertex is reliably broadcast) "rdlver", it adds it to its local
  set of "buffered vertices" via the "receive vertex" action. This set contains all the vertices that have been received but have
  not yet been integrated into the node's local copy of the DAG.
  Then, whenever it is possible to add to that DAG a buffered vertex, the node do so, via the "add to DAG" action.
  Several vertices may be added in a cascading effect once a new vertex is received.
- the DagRider ordering layer (in dark green on the diagram) is tasked with analyzing the structure of the (local copy of the) DAG
  in order to sort the vertices that it contains and thus provide the ledger's total ordering of transactions.
  To do so, "leader vertices" are regularly chosen (every 4 columns in the DAG). The choice of these leaders is made by relying on a
  global coin layer (in orange on the diagram). Once a leader is chosen, all the vertices in its causal sub graph (i.e. all the vertices of the DAG that are
  reachable from it), minus the vertices that were already ordered previously, are ordered deterministically and delivered (via the "order vertex" action),
  which triggers the delivery (application side) of the transactions they contain (via the "deliver transaction" action).
  This process is done regularly (every four columns) as the DAG grows. Each new set of vertices that is added is called a wave.
- the global coin layer (in orange on the diagram) is tasked with the selection of leader vertices of each wave.
  It relies on cryptographic techniques to makes so that the leader selection is deterministic w.r.t. 
  the content of the DAG but cannot be predicted too long in advance by an adversary.



## Implementation of the model

The application layer is left generic, as per the interface provided in 
[max.model.ledger.abstract_ledger](https://gitlab.com/cea-licia/max/models/ledgers/max.model.ledger.abstract_ledger).

The DagRider layers are both implemented in this present model/repository.

The deterministic order used to order vertices that are part of the same wave is handled via the
*AbstractInWaveDeterministicOrder* interface.
The modeller is free to define an ad-hoc order or to use one of the two builtins:
- *DefaultInWaveDeterministicOrder* which orders vertices round by round, randomly shuffling vertices of the same round.
- *VoteCountInWaveDeterministicOrder* which orders vertices as per a score value computed via reasoning on a number of votes, à la [Board & Clerk](https://www.computer.org/csdl/proceedings-article/ispa-bdcloud-socialcom-sustaincom/2023/292200a294/1W3wuoDzX0Y). Likewise, if several vertices have the same score, they are randomly shuffled.

The reliable broadcast layer is left generic, as per the interface provided in 
[max.model.broadcast.abstract_reliable_broadcast](https://gitlab.com/cea-licia/max/models/broadcasts/max.model.broadcast.abstract_reliable_broadcast).
For instance, we can use the model implementing Bracha's Byzantine Reliable Broadcast:
[max.model.broadcast.bracha_brb](https://gitlab.com/cea-licia/max/models/broadcasts/max.model.broadcast.bracha_brb).

The global coin layer is abstracted away via PRNG with a seed that depends on the wave number so that its "agreement" property 
(from "[All you need is DAG](https://dl.acm.org/doi/10.1145/3465084.3467905)") is ensured.



## Simple example simulations

We consider a simple system with 4 DagRider nodes and 4 clients, each client only sending transaction to one specific DagRider
(i.e. there is a node per client / a client per node).

For the sake of simplicity, we use [max.model.broadcast.trivial_broadcast](https://gitlab.com/cea-licia/max/models/broadcasts/max.model.broadcast.trivial_broadcast)
as a concrete implementation of the reliable broadcast layer (via [max.model.broadcast.abstract_reliable_broadcast](https://gitlab.com/cea-licia/max/models/broadcasts/max.model.broadcast.abstract_reliable_broadcast)).

Clients regularly send transactions of the form "(clientID,integer)" with integers of increasing values.

If we simulate the system with a perfect network (no loss and no communication delays), the local DAGs on each of the four nodes have
the structure exemplified below:

<img src="./README_images/simu_perfect.png">

Because we each client emits a new transaction every 10 ticks of simulation, and because we have a simulation of 200 ticks, each client emits a total of 20
transactions. Because the broadcast of vertices is immediate and the nodes are parameterized so as not to emit empty vertices,
we have one vertex per transaction and thus 20 columns in our DAGs.
It is as if the system is entirely lock-step synchronous. There are always 4 strong edges exiting each vertex and no weak edges.

By contrast, if we add random delays in network communications (more precisely, we configure the network under the reliable broadcast layer
[max.model.broadcast.abstract_reliable_broadcast](https://gitlab.com/cea-licia/max/models/broadcasts/max.model.broadcast.abstract_reliable_broadcast)
using the interface provided in 
[max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p)),
the aspect of the DAGs is rather exemplified by the diagram below:

<img src="./README_images/simu_delay.png">



## Example simulations with the Bracha algorithm as the underlying Byzantine Reliable Broadcast


If we use Bracha's Byzantine Reliable Broadcast:
[max.model.broadcast.bracha_brb](https://gitlab.com/cea-licia/max/models/broadcasts/max.model.broadcast.bracha_brb) as the underlying Byzantine Reliable Broadcast algorithm, we can have more realistic simulations.

Because of the underlying Peer To Peer network producing random delays between emissions and receptions,
we are not guaranteed to deliver vertices
(`rdlver` operation of the schema with the layers of DagRider above)
in the same order as they are emitted by the nodes (`rbcast` operation in that same schema).

Also, it is possible for a given node to emit several vertex proposals (via successive calls to `rbcast`)
without at first delivering its previous vertices (via `rdlver`).
This may result in the following :

<img src="./README_images/simu_bracha.png">

We can see on several occasions that there
are no strong edges between consecutive vertices of the same row.

In particular, this can cause, on the same row *r*, to have a vertex in a certain column *c* included in a wave *w*
while its immediate neighbor at row *r* and column *c-1* is in wave *w+1*.



### Running the simulations

In order to reproduce the simulations:
- run the test in "DrTrivialBroadcastHonestNodesPerfectNetworkTest" to run the simulation with a prefect network
- run the test in "DrTrivialBroadcastHonestNodesDelayedNetworkTest" to run the simulation with delays
- run the test in "DrBrachaBrbHonestNodesDelayedNetworkTest" to run the simulation with Bracha as the reliable broadcast algorithm

In order to regenerate the DAG drawings as above, in each of "DrHonestNodesPerfectNetworkTest" and "DrHonestNodesDelayedNetworkTest":
- set the variable "drawDAGs" to *true*
- specify the path of the folder in which you want to generate the images via the "pathPrefix" variable




# Licence

__max.model.ledger.dag_rider__ is released under
the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)