open module max.model.ledger.dag_rider {
    // Exported packages
    exports max.model.ledger.dag_rider.action.config;
    exports max.model.ledger.dag_rider.action.protocol;
    exports max.model.ledger.dag_rider.agent;
    exports max.model.ledger.dag_rider.behavior;
    exports max.model.ledger.dag_rider.behavior.graph;
    exports max.model.ledger.dag_rider.behavior.handler;
    exports max.model.ledger.dag_rider.behavior.hash;
    exports max.model.ledger.dag_rider.behavior.inwave_deterministic_order;
    exports max.model.ledger.dag_rider.behavior.inwave_deterministic_order.vote_count;
    exports max.model.ledger.dag_rider.behavior.proposer;
    exports max.model.ledger.dag_rider.draw;
    exports max.model.ledger.dag_rider.env;
    exports max.model.ledger.dag_rider.error;
    exports max.model.ledger.dag_rider.role;
    exports max.model.ledger.dag_rider.usecase.puzzle.action;
    exports max.model.ledger.dag_rider.usecase.puzzle.mockup;

    // Dependencies
    requires java.logging;
    requires java.desktop;
    requires org.apache.commons.lang3;
    requires org.jgrapht.core;

    requires madkit;      // Automatic

    requires max.core;
    requires max.datatype.com;
    requires max.model.network.stochastic_adversarial_p2p;
    requires max.model.ledger.abstract_ledger;
    requires max.model.broadcast.abstract_reliable_broadcast;

    requires one.util.streamex;
    // Optional dependency (required for tests)
    requires static org.junit.jupiter.api;
    requires static org.junit.jupiter.params;

}