/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/


package max.model.ledger.dag_rider.error;


/**
 * An exception related to malformed DagRider Vertex Proposals
 *
 * @author Erwan Mahe
 * **/
public class MalformedDagVertexProposalException extends Exception {
    public MalformedDagVertexProposalException(String errorMessage) {
        super(errorMessage);
    }
}