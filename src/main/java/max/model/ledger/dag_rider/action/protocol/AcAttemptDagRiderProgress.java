/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.action.protocol;

import max.core.action.Action;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.hash.DrAbstractHasherForParentSecuring;
import max.model.ledger.dag_rider.behavior.hash.DrHasherSingleton;
import max.model.ledger.dag_rider.env.DagRiderContext;
import max.model.ledger.dag_rider.role.RDagRiderNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Action executed in order to progress at the dag construction.
 *
 * @author Erwan Mahe
 */
public class AcAttemptDagRiderProgress<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends Action<DagRiderAgent> {

    public AcAttemptDagRiderProgress(String drEnvironmentName, DagRiderAgent owner) {
        super(drEnvironmentName, RDagRiderNode.class, owner);
    }

    @Override
    public void execute() {
        DagRiderContext<T_tx,T_st> context = (DagRiderContext<T_tx,T_st>) this.getOwner().getContext(this.getEnvironment());
        // ***
        // uncomment for debugging the building of the graph
        //getOwner().getLogger().info(context.get_DR_graph().toString());
        // ***
        if (context.get_DR_current_round() % 4 == 0) {
            new AcProcessWave<>(this.getEnvironment(), getOwner(), context.get_DR_current_round() / 4).execute();
        }
        new AcGoToNextRoundAndPropose<>(this.getEnvironment(), getOwner()).execute();
    }

    @Override
    public <T extends Action<DagRiderAgent>> T copy() {
        return (T) new AcAttemptDagRiderProgress(getEnvironment(), getOwner());
    }
}
