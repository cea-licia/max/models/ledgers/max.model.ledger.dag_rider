/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.action.config;

import max.core.action.Action;
import max.model.broadcast.abstract_reliable_broadcast.action.config.AcSetBroadcastDeliveryHandler;
import max.model.ledger.abstract_ledger.action.config.AcSetLocalApprovalPolicy;
import max.model.ledger.abstract_ledger.action.config.AcSetLocalLedgerState;
import max.model.ledger.abstract_ledger.approval.AbstractTransactionApprovalPolicy;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.inwave_deterministic_order.AbstractInWaveDeterministicOrder;
import max.model.ledger.dag_rider.behavior.proposer.DagRiderVertexProposer;
import max.model.ledger.dag_rider.env.DagRiderContext;
import max.model.ledger.dag_rider.env.DagRiderEnvironment;
import max.model.ledger.dag_rider.behavior.handler.DagRiderVertexProposalOnBroadcastDeliveryHandler;
import max.model.ledger.dag_rider.role.RDagRiderNode;
import max.model.network.stochastic_adversarial_p2p.action.config.AcAddCommunicationDelay;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.context.filter.UniformLevelFilter;

import java.util.Optional;

/**
 * Action to setup a DagRider node.
 *
 * @author Erwan Mahe
 */
public class AcSetupDagRiderNode<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends Action<DagRiderAgent> {


    private final T_st initialLedgerState;

    private final AbstractTransactionApprovalPolicy<T_tx,T_st> drTransactionsApprovalPolicy;

    private final Optional<DelaySpecification> baseline_output_delay;
    private final Optional<DelaySpecification> baseline_input_delay;

    private final DagRiderVertexProposer<T_tx> vertexProposer;

    private final AbstractInWaveDeterministicOrder<T_tx> deterministicOrder;



    public AcSetupDagRiderNode(String drEnvironmentName,
                               DagRiderAgent owner,
                               T_st initialLedgerState,
                               AbstractTransactionApprovalPolicy<T_tx,T_st> drTransactionsApprovalPolicy,
                               Optional<DelaySpecification> baseline_output_delay,
                               Optional<DelaySpecification> baseline_input_delay,
                               DagRiderVertexProposer<T_tx> vertexProposer,
                               AbstractInWaveDeterministicOrder<T_tx> deterministicOrder) {
        super(drEnvironmentName, RDagRiderNode.class, owner);
        this.initialLedgerState = initialLedgerState;
        this.drTransactionsApprovalPolicy = drTransactionsApprovalPolicy;
        this.baseline_output_delay = baseline_output_delay;
        this.baseline_input_delay = baseline_input_delay;
        this.vertexProposer = vertexProposer;
        this.deterministicOrder = deterministicOrder;
    }

    @Override
    public void execute() {
        DagRiderContext<T_tx,T_st> context = (DagRiderContext<T_tx,T_st>) this.getOwner().getContext(this.getEnvironment());
        DagRiderEnvironment<T_tx,T_st> drEnv = (DagRiderEnvironment<T_tx,T_st>) context.getEnvironment();

        (new AcSetLocalLedgerState<T_tx,T_st,DagRiderAgent>(
                this.getEnvironment(),
                this.getOwner(),
                this.initialLedgerState
        )).execute();

        (new AcSetLocalApprovalPolicy<T_tx,T_st,DagRiderAgent>(
                this.getEnvironment(),
                this.getOwner(),
                this.drTransactionsApprovalPolicy
        )).execute();

        context.vertexProposer = this.vertexProposer;

        context.deterministicOrder = deterministicOrder;

        (new AcSetBroadcastDeliveryHandler<DagVertex<T_tx>,DagRiderAgent>(
                drEnv.reliableBroadcastEnvironmentName,
                this.getOwner(),
                new DagRiderVertexProposalOnBroadcastDeliveryHandler<T_tx>())).execute();

        this.baseline_output_delay.ifPresent(delay -> {
            (new AcAddCommunicationDelay<DagRiderAgent>(
                    drEnv.reliableBroadcastEnvironmentName,
                    this.getOwner(),
                    "OUTPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay)).execute();
        });
        this.baseline_input_delay.ifPresent(delay -> {
            (new AcAddCommunicationDelay<DagRiderAgent>(
                    drEnv.reliableBroadcastEnvironmentName,
                    this.getOwner(),
                    "INPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    delay)).execute();
        });

    }

    @Override
    public <T extends Action<DagRiderAgent>> T copy() {
        return (T) new AcSetupDagRiderNode(getEnvironment(), getOwner(),
                this.initialLedgerState,
                this.drTransactionsApprovalPolicy,
                this.baseline_output_delay,
                this.baseline_input_delay,
                this.vertexProposer,
                this.deterministicOrder);
    }
}
