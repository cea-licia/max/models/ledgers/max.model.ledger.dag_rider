/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.action.protocol;

import max.core.action.Action;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.hash.DrAbstractHasherForParentSecuring;
import max.model.ledger.dag_rider.behavior.hash.DrHasherSingleton;
import max.model.ledger.dag_rider.env.DagRiderContext;
import max.model.ledger.dag_rider.role.RDagRiderNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Action executed when the agent playing the DAgRiderNode role
 * tries to add a new vertex to its local copy of the DAG
 *
 * @author Erwan Mahe
 */
public class AcAddToDAG<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends Action<DagRiderAgent> {

    public AcAddToDAG(String drEnvironmentName, DagRiderAgent owner) {
        super(drEnvironmentName, RDagRiderNode.class, owner);
    }

    @Override
    public void execute() {
        getOwner().getLogger().info("Trying adding new vertices to DAG");
        DagRiderContext<T_tx,T_st> context = (DagRiderContext<T_tx,T_st>) this.getOwner().getContext(this.getEnvironment());
        DrAbstractHasherForParentSecuring<T_tx> hasher = (DrAbstractHasherForParentSecuring<T_tx>) DrHasherSingleton.getInstance().get_concrete_hasher_for_DR_environment(this.getEnvironment());
        int num_added_vertices = 0;
        my_while : while (true) {
            List<DagVertex<T_tx>> to_remove = new ArrayList<>();
            // ***
            browse_props : for (DagVertex<T_tx> prop : context.get_DR_received_proposals()) {
                my_switch : switch (context.get_DR_graph().can_add_vertex(prop, hasher, context.byzantineThresholdF)) {
                    case CanAddToDAG :
                        to_remove.add(prop);
                        context.get_DR_graph().add_vertex(prop);
                        getOwner().getLogger().info("Added vertex to DAG : from" + prop.coordinates);
                        num_added_vertices += 1;
                        break browse_props;
                    case UninitializedDAG:
                        getOwner().getLogger().severe("Uninitialized DAG when AcAddToDAG : round : " + context.get_DR_current_round());
                        throw new RuntimeException("executed AcAddToDAG before initializing DAG");
                    case UnknownProposer:
                        to_remove.add(prop);
                        getOwner().getLogger().info("Found unexpected vertex in received proposals : proposal from unknown proposer " + prop.coordinates.proposer_node);
                        break my_switch;
                    case MalformedVertex:
                        to_remove.add(prop);
                        getOwner().getLogger().info("Found unexpected vertex in received proposals : malformed vertex (strong edges : " + prop.strong_edges + " - weak edges : " + prop.weak_edges + ")");
                        break my_switch;
                    case CoordinatesAlreadyOccupied:
                        to_remove.add(prop);
                        getOwner().getLogger().info("Found unexpected vertex in received proposals : DAG already contains a vertex at position " + prop.coordinates);
                        break my_switch;
                    case MissingEdgeTarget:
                        break my_switch;
                    case WrongHashCodeForEdgeTarget:
                        to_remove.add(prop);
                        getOwner().getLogger().info("Found unexpected vertex in received proposals : edge target reference has wrong hashcode w.r.t. actual DAG content");
                        break my_switch;
                }
            }
            // ***
            if (!to_remove.isEmpty()) {
                to_remove.forEach(context.get_DR_received_proposals()::remove);
            } else {
                break my_while;
            }
        }
        if (num_added_vertices == 0) {
            getOwner().getLogger().info("Could not add any new vertices to DAG");
        } else {
            getOwner().getLogger().info("Added " + num_added_vertices + " new vertices to the DAG");
            // ***
            int num_vertices_at_current_round = context.vertexProposer.count_vertices_at_round(context.get_DR_current_round(), context.get_DR_graph());
            int threshold = (2*context.byzantineThresholdF) + 1;
            if (num_vertices_at_current_round >= threshold) {
                new AcAttemptDagRiderProgress<>(this.getEnvironment(), getOwner()).execute();
            }
        }
    }

    @Override
    public <T extends Action<DagRiderAgent>> T copy() {
        return (T) new AcAddToDAG(getEnvironment(), getOwner());
    }
}
