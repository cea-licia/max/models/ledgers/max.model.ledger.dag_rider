/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.action.protocol;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.env.DagRiderContext;
import max.model.ledger.dag_rider.role.RDagRiderNode;

import java.util.HashSet;
import java.util.Set;

/**
 * Action executed when the agent playing the DAgRiderNode role initializes its DAG at genesis.
 *
 * @author Erwan Mahe
 */
public class AcInitializeDAG<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends Action<DagRiderAgent> {


    public AcInitializeDAG(String drEnvironmentName, DagRiderAgent owner) {
        super(drEnvironmentName, RDagRiderNode.class, owner);
    }

    @Override
    public void execute() {
        DagRiderContext<T_tx,T_st> context = (DagRiderContext<T_tx,T_st>) this.getOwner().getContext(this.getEnvironment());

        context.get_DR_graph().setLogger(getOwner().getLogger());

        // DagRider members
        Set<String> proposer_nodes = new HashSet<String>();
        proposer_nodes.add(
                context.getMyAddress(RDagRiderNode.class.getName()).getAgent().getName()
        );
        for (AgentAddress agentAddress : this.getOwner().getAgentsWithRole(this.getEnvironment(), RDagRiderNode.class)) {
            proposer_nodes.add(agentAddress.getAgent().getName());
        }

        int f = proposer_nodes.size() / 3;
        if (3*f + 1 == proposer_nodes.size()) {
            context.byzantineThresholdF = f;
        } else {
            throw new RuntimeException("could not compute Byzantine threshold : num nodes is " + proposer_nodes.size() +" f is " + f);
        }
        context.initialize_dag(proposer_nodes);
        getOwner().getLogger().info("Initialized DAG data structure with genesis vertices");
        // ***
        getOwner().getLogger().info(context.get_DR_graph().toString());
        // ***
        context.increment_DR_current_round();
        getOwner().getLogger().info("Set current round to : " + context.get_DR_current_round());
    }

    @Override
    public <T extends Action<DagRiderAgent>> T copy() {
        return (T) new AcInitializeDAG(getEnvironment(), getOwner());
    }
}
