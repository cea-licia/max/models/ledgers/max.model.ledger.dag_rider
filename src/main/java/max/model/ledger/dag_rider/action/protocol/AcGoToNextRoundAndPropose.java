/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.action.protocol;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.model.broadcast.abstract_reliable_broadcast.action.logic.AcStartBroadcastOfMessage;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.graph.DagVertexCoordinates;
import max.model.ledger.dag_rider.behavior.hash.DrAbstractHasherForParentSecuring;
import max.model.ledger.dag_rider.behavior.hash.DrHasherSingleton;
import max.model.ledger.dag_rider.env.DagRiderContext;
import max.model.ledger.dag_rider.env.DagRiderEnvironment;
import max.model.ledger.dag_rider.role.RDagRiderNode;

import java.util.*;


/**
 * Action executed when the agent playing the DAgRiderNode role proposes a new vertex and goes to the next round.
 *
 * @author Erwan Mahe
 */
public class AcGoToNextRoundAndPropose<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends Action<DagRiderAgent> {


    public AcGoToNextRoundAndPropose(String drEnvironmentName, DagRiderAgent owner) {
        super(drEnvironmentName, RDagRiderNode.class, owner);
    }

    @Override
    public void execute() {
        DagRiderContext<T_tx,T_st> context = (DagRiderContext<T_tx,T_st>) this.getOwner().getContext(this.getEnvironment());
        DrAbstractHasherForParentSecuring<T_tx> hasher = (DrAbstractHasherForParentSecuring<T_tx>) DrHasherSingleton.getInstance().get_concrete_hasher_for_DR_environment(this.getEnvironment());
        // ***
        int current_round = context.get_DR_current_round();
        AgentAddress my_address = context.getMyAddress(RDagRiderNode.class.getName());
        String my_agentName = my_address.getAgent().getName();
        // ***
        getOwner().getLogger().info("Will try to propose : current round : " + current_round);
        // ***
        switch (context.get_DR_graph().can_make_new_proposal(my_agentName,current_round,context.vertexProposer, context.byzantineThresholdF)) {
            case CanProposeNewVertex:
                getOwner().getLogger().info("Proposing new vertex for round " + context.get_DR_current_round());
                List<T_tx> transactions = context.clientTransactionsMempool.extractTransactionsForProposal(
                        Optional.empty(),
                        Optional.empty(),
                        context.approvalPolicy,
                        context.ledgerValidatorState);
                if(context.vertexProposer.stall_proposal_depending_on_txs_extracted_from_mempool(transactions)) {
                    getOwner().getLogger().info("Stalling vertex proposal until content is collected");
                    return;
                }
                // ***
                this.getOwner().getLogger().info("purging local mempool from transactions that are in the new vertex proposal (to mitigate duplicated transactions)");
                context.clientTransactionsMempool.purgeMempoolOfDeliveredBlock(
                        transactions
                );
                // ***
                DagVertexCoordinates proposalCoords = new DagVertexCoordinates(current_round,my_agentName);
                DagVertex<T_tx> proposal = context.vertexProposer.make_new_proposal(proposalCoords,transactions,hasher,context.get_DR_graph(), context.byzantineThresholdF);
                DagRiderEnvironment<T_tx,T_st> drEnv = (DagRiderEnvironment<T_tx,T_st>) context.getEnvironment();
                (new AcStartBroadcastOfMessage<>(drEnv.reliableBroadcastEnvironmentName, getOwner(), proposal)).execute();
                // ***
                context.increment_DR_current_round();
                getOwner().getLogger().info("Updated current round to " + context.get_DR_current_round());
                break;
            case UninitializedDAG:
                getOwner().getLogger().severe("Uninitialized DAG when AcGoToNextRoundAndPropose : round : " + context.get_DR_current_round());
                throw new RuntimeException("executed AcGoToNextRoundAndPropose before initializing DAG");
            case RoundIsZeroOrLess:
                throw new RuntimeException("unexpected round value in AcGoToNextRoundAndPropose");
            case NotEnoughPossibleStrongEdges:
                getOwner().getLogger().info("Abort AcGoToNextRoundAndPropose : not enough vertices for current round in DAG");
                break;
        }
    }

    @Override
    public <T extends Action<DagRiderAgent>> T copy() {
        return (T) new AcGoToNextRoundAndPropose(getEnvironment(), getOwner());
    }
}
