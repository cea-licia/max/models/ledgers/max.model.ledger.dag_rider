/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.action.protocol;

import max.core.action.Action;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.behavior.graph.DagVertexCoordinates;
import max.model.ledger.dag_rider.env.DagRiderContext;
import max.model.ledger.dag_rider.role.RDagRiderNode;

import java.util.List;

/**
 * Action executed when the agent playing the DAgRiderNode role processes a new wave.
 *
 * @author Erwan Mahe
 */
public class AcProcessWave<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends Action<DagRiderAgent> {

    private final int wave_number;

    public AcProcessWave(String drEnvironmentName, DagRiderAgent owner, int wave_number) {
        super(drEnvironmentName, RDagRiderNode.class, owner);
        this.wave_number = wave_number;
    }

    @Override
    public void execute() {
        getOwner().getLogger().info("AcProcessWave");
        DagRiderContext<T_tx,T_st> context = (DagRiderContext<T_tx,T_st>) this.getOwner().getContext(this.getEnvironment());

        if (context.get_decided_wave() < this.wave_number) {

            int next_wave_to_try = this.wave_number;
            while (next_wave_to_try > context.get_decided_wave()) {
                List<DagVertexCoordinates> leader_stack = context.get_leader_stack_from_wave(next_wave_to_try);
                if (leader_stack.isEmpty()) {
                    getOwner().getLogger().info("wave " + next_wave_to_try + " cannot be processed : empty leader stack");
                    next_wave_to_try -= 1;
                } else {
                    context.order_and_deliver_vertices(leader_stack);
                    // ***
                    context.set_decided_wave(next_wave_to_try);
                    getOwner().getLogger().info("updated decided wave as " + next_wave_to_try);
                    break;
                }
            }

        } else {
            getOwner().getLogger().info("Already processed wave number " + this.wave_number);
        }
    }

    @Override
    public <T extends Action<DagRiderAgent>> T copy() {
        return (T) new AcProcessWave(getEnvironment(), getOwner(),this.wave_number);
    }
}
