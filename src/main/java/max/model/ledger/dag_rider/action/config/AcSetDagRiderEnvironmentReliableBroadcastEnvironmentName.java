/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.action.config;

import max.core.action.Action;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.dag_rider.env.DagRiderEnvironment;
import max.model.ledger.dag_rider.role.RDagRiderChannel;


/**
 * Action to notify a DagRiderEnvironment of the name of the Reliable Broadcast Environment it will leverage
 * for reliable broadcast of vertices.
 *
 * @author Erwan Mahe
 */
public class AcSetDagRiderEnvironmentReliableBroadcastEnvironmentName<T_tx, T_st extends AbstractLocalLedgerState<T_tx>>
        extends Action<DagRiderEnvironment<T_tx, T_st>> {


    private final String reliableBroadcastEnvironmentName;

    public AcSetDagRiderEnvironmentReliableBroadcastEnvironmentName(String drEnvironmentName,
                                                                    DagRiderEnvironment<T_tx, T_st> owner,
                                                                    String reliableBroadcastEnvironmentName) {
        super(drEnvironmentName, RDagRiderChannel.class, owner);
        this.reliableBroadcastEnvironmentName = reliableBroadcastEnvironmentName;
    }

    @Override
    public void execute() {
        this.getOwner().reliableBroadcastEnvironmentName = this.reliableBroadcastEnvironmentName;
    }

    @Override
    public <T extends Action<DagRiderEnvironment<T_tx, T_st>>> T copy() {
        return (T) new AcSetDagRiderEnvironmentReliableBroadcastEnvironmentName(getEnvironment(), getOwner(),
                this.reliableBroadcastEnvironmentName);
    }
}
