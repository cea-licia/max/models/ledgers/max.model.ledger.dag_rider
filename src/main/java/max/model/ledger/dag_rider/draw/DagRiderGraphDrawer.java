/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.draw;

import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.graph.DagVertexCoordinates;
import max.model.ledger.dag_rider.behavior.graph.DagVertexReference;
import max.model.ledger.dag_rider.env.DagRiderContext;
import org.apache.commons.lang3.tuple.Pair;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import java.util.function.Function;

import javax.imageio.ImageIO;


/**
 * Draws a DagRider local DAG in a PNG image
 *
 * @author Erwan Mahe
 */
public class DagRiderGraphDrawer {

    private static Color[] wave_colors = {
            // variant 1
            new Color(100, 150, 250),
            new Color(150, 250, 100),
            new Color(250, 100, 150),
            // ***
            new Color(100, 250, 150),
            new Color(250, 150, 100),
            new Color(150, 100, 250),
            // ***
            // variant 2
            new Color(250, 200, 50),
            new Color(50, 250, 200),
            new Color(200, 50, 250),
            // ***
            new Color(50, 200, 250),
            new Color(250, 50, 200),
            new Color(200, 250, 50),
    };


    public static <T_tx, T_st extends AbstractLocalLedgerState<T_tx>> void drawDrGraph(
            DagRiderContext<T_tx, T_st> drContext,
            String name,
            Function<T_tx, String> transactionPrinter,
            String pathPrefix,
            HashMap<String, ArrayList<String>> validatorNodesLabelsToDraw) {
        Font font = new Font("Monospaced", Font.PLAIN, 20);

        ExtractedDrGraphLayoutInformation<T_tx> layout = ExtractedDrGraphLayoutInformation.extract_from_drGraph(
                validatorNodesLabelsToDraw,
                drContext.get_DR_graph(),
                transactionPrinter,
                font
        );

        HashSet<DagVertexCoordinates> leadersCoordinates = new HashSet<>();
        for (int w = 1; w <= drContext.get_decided_wave();w++) {
            leadersCoordinates.add(drContext.get_DR_graph().get_wave_vertex_leader_coordinates(w));
        }

        try {
            int imageWidth = layout.get_image_width();
            int imageHeight = layout.get_image_height();
            // TYPE_INT_ARGB specifies the image format: 8-bit RGBA packed
            // into integer pixels
            BufferedImage bi = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);

            Graphics2D ig2 = bi.createGraphics();


            ig2.setFont(font);
            FontMetrics fontMetrics = ig2.getFontMetrics();

            ig2.setPaint(Color.WHITE);
            ig2.fillRect(0,0,imageWidth,imageHeight);

            List<String> agentsNames = Arrays.asList(drContext.get_DR_graph().get_agents_lexicographic());

            // draw edges
            for (DagVertexCoordinates coordinates : drContext.get_DR_graph().vertices.keySet()) {
                Pair<Integer,Integer> origin = layout.getVertexPlanarCoordinates(coordinates,agentsNames);
                int originX = origin.getLeft();
                int originY = origin.getRight();
                DagVertex<T_tx> vertex = drContext.get_DR_graph().get_vertex_at_position(coordinates);
                for (DagVertexReference reference : vertex.strong_edges) {
                    //System.out.println("drawing edge between " + coordinates + " and " + reference.coordinates);
                    Pair<Integer,Integer> target = layout.getVertexPlanarCoordinates(reference.coordinates,agentsNames);
                    int targetX = target.getLeft();
                    int targetY = target.getRight();
                    //System.out.println("(" + originX + "," + originY + ") -> (" + targetX + "," + targetY + ")");
                    ig2.setPaint(Color.RED);
                    ig2.setColor(Color.RED);
                    ig2.drawLine(originX,originY,targetX,targetY);
                }
                for (DagVertexReference reference : vertex.weak_edges) {
                    Pair<Integer,Integer> target = layout.getVertexPlanarCoordinates(reference.coordinates,agentsNames);
                    int targetX = target.getLeft();
                    int targetY = target.getRight();
                    ig2.setPaint(Color.BLUE);
                    ig2.setColor(Color.BLUE);
                    ig2.drawLine(originX,originY,targetX,targetY);
                }
            }

            // draw vertices


            for (DagVertexCoordinates coordinates : drContext.get_DR_graph().vertices.keySet()) {
                Pair<Integer,Integer> vert = layout.getVertexPlanarCoordinates(coordinates,agentsNames);
                int vertX = vert.getLeft();
                int vertY = vert.getRight();
                //System.out.println("drawing vertex at coordinates " + coordinates);
                //System.out.println("drawing vertex at " + vertX + "," + vertY);
                // ***
                Pair<Integer,Integer> vertSpace = layout.getVertexWidthAndHeight(coordinates);
                int vertexWidth = vertSpace.getLeft();
                int vertexHeight = vertSpace.getRight();
                // ***
                Color rect_fill_color = null;
                if (coordinates.vertex_round == 0) {
                    rect_fill_color = Color.BLACK;
                } else {
                    if (drContext.wave_map.containsKey(coordinates)){
                        int wave_number = drContext.wave_map.get(coordinates);
                        rect_fill_color = DagRiderGraphDrawer.wave_colors[wave_number % DagRiderGraphDrawer.wave_colors.length];
                    } else {
                        rect_fill_color = Color.WHITE;
                    }
                }
                ig2.setPaint(rect_fill_color);
                ig2.fillRect(vertX - vertexWidth/2,vertY - vertexHeight/2,vertexWidth,vertexHeight);
                // ***
                Color rect_border_color = null;
                Stroke rect_border_stroke = null;
                Stroke original_stroke = ig2.getStroke();
                if (coordinates.vertex_round == 0) {
                    rect_border_color = Color.CYAN;
                    rect_border_stroke = new BasicStroke(7);
                } else {
                    if (drContext.wave_map.containsKey(coordinates)){
                        if (leadersCoordinates.contains(coordinates)) {
                            rect_border_color = Color.RED;
                            rect_border_stroke = new BasicStroke(7);
                        } else {
                            rect_border_color = Color.BLACK;
                            rect_border_stroke = original_stroke;
                        }
                    } else {
                        rect_border_color = Color.BLACK;
                        rect_border_stroke = original_stroke;
                    }
                }
                ig2.setColor(rect_border_color);
                ig2.setStroke(rect_border_stroke);
                ig2.drawRect(vertX - vertexWidth/2,vertY - vertexHeight/2,vertexWidth,vertexHeight);
                ig2.setStroke(original_stroke);
                // ***
                List<String> string_content = layout.stringContentPerVertex.get(coordinates);
                int lineX = vertX;
                int lineY = vertY - vertexHeight/2;
                if (coordinates.vertex_round == 0) {
                    ig2.setPaint(Color.WHITE);
                } else {
                    ig2.setPaint(Color.BLACK);
                }
                for (String line : string_content) {
                    int stringWidth = fontMetrics.stringWidth(line);
                    int stringHeight = fontMetrics.getAscent();
                    ig2.drawString(line, lineX - stringWidth/2, lineY + stringHeight);
                    lineY += stringHeight;
                }
                ig2.setPaint(Color.BLACK);
            }

            ig2.dispose();

            String filePath = pathPrefix + name + ".PNG";
            ImageIO.write(bi, "PNG", new File(filePath));

        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }
}