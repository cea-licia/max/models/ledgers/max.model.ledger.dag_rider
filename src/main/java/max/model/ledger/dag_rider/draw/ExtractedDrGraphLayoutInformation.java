/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.draw;

import max.model.ledger.dag_rider.behavior.graph.DagRiderGraph;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.graph.DagVertexCoordinates;
import org.apache.commons.lang3.tuple.Pair;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;


/**
 * Extracts layout information from a DAG Rider local DAG in order to then draw it.
 *
 * @author Erwan Mahe
 */
public class ExtractedDrGraphLayoutInformation<T_tx> {
    public HashMap<Integer,Integer> maxCharWidthPerRound;
    public HashMap<String,Integer> maxLineHeightPerNode;

    public HashMap<DagVertexCoordinates,List<String>> stringContentPerVertex;

    public int charWidth;
    public int lineHeight;

    public ExtractedDrGraphLayoutInformation(
            HashMap<Integer, Integer> maxCharWidthPerRound,
            HashMap<String, Integer> maxLineHeightPerNode,
            HashMap<DagVertexCoordinates,
                    List<String>> stringContentPerVertex,
            int charWidth,
            int lineHeight) {
        this.maxCharWidthPerRound = maxCharWidthPerRound;
        this.maxLineHeightPerNode = maxLineHeightPerNode;
        this.stringContentPerVertex = stringContentPerVertex;
        this.charWidth = charWidth;
        this.lineHeight = lineHeight;
    }

    public static <T_tx> ExtractedDrGraphLayoutInformation<T_tx> extract_from_drGraph(
            HashMap<String,ArrayList<String>> validatorNodesLabelsToDraw,
            DagRiderGraph<T_tx> drGraph,
            Function<T_tx, String> transactionPrinter,
            Font font) {

        BufferedImage sizeImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D sizeGraphics = (Graphics2D) sizeImage.getGraphics();

        FontMetrics fontMetrics = sizeGraphics.getFontMetrics(font);
        char mychar = 'a';
        int charWidth = fontMetrics.charWidth(mychar);
        if (charWidth <= 0) {
            charWidth = 1;
        }
        int lineHeight = fontMetrics.getHeight();
        if (lineHeight <= 0) {
            lineHeight = font.getSize();
        }


        HashMap<Integer,Integer> maxCharWidthPerRound = new HashMap<>();
        HashMap<String,Integer> maxLineHeightPerNode = new HashMap<>();
        for (String agentName : drGraph.get_agents_lexicographic()) {
            maxLineHeightPerNode.put(agentName,0);
        }

        HashMap<DagVertexCoordinates,List<String>> stringContentPerVertex = new HashMap<>();

        for (Map.Entry<DagVertexCoordinates,DagVertex<T_tx>> entry : drGraph.vertices.entrySet()) {
            // ***
            DagVertex<T_tx> vertex = entry.getValue();
            DagVertexCoordinates coords = entry.getKey();
            // ***
            maxCharWidthPerRound.merge(coords.vertex_round,0,Integer::max);
            // ***
            List<String> stringLinesToDrawInVertex = new ArrayList<>();
            if (coords.vertex_round == 0) {
                if(validatorNodesLabelsToDraw.containsKey(coords.proposer_node)) {
                    for (String line : validatorNodesLabelsToDraw.get(coords.proposer_node)) {
                        maxCharWidthPerRound.merge(coords.vertex_round,line.length(),Integer::max);
                        stringLinesToDrawInVertex.add(line);
                    }
                }
            } else {
                for (T_tx tr : vertex.transactions) {
                    String line = transactionPrinter.apply(tr);
                    maxCharWidthPerRound.merge(coords.vertex_round,line.length(),Integer::max);
                    stringLinesToDrawInVertex.add(line);
                }
            }
            // ***
            maxLineHeightPerNode.merge(coords.proposer_node,stringLinesToDrawInVertex.size(),Integer::max);
            stringContentPerVertex.put(coords,stringLinesToDrawInVertex);
        }
        return new ExtractedDrGraphLayoutInformation<T_tx>(maxCharWidthPerRound,maxLineHeightPerNode,stringContentPerVertex,charWidth,lineHeight);
    }


    private static int paddingBetweenVertices = 20;

    private static int paddingToBorder = 20;

    private static int minimalVertexSize = 50;

    public int get_image_width() {
        int imageWidth = paddingToBorder;
        for (Integer num_chars : this.maxCharWidthPerRound.values()) {
            imageWidth += Integer.max(this.charWidth*num_chars,minimalVertexSize) + paddingBetweenVertices;
        }
        return imageWidth;
    }

    public int get_image_height() {
        int imageHeight = paddingToBorder;
        for (Integer lines_num : this.maxLineHeightPerNode.values()) {
            imageHeight += Integer.max(this.lineHeight*lines_num,minimalVertexSize) + paddingBetweenVertices;
        }
        return imageHeight;
    }

    public Pair<Integer,Integer> getVertexPlanarCoordinates(DagVertexCoordinates coords, List<String> agentsNames) {
        int x = paddingToBorder/2;
        for (int column = 0; column < coords.vertex_round ; column ++) {
            x += Integer.max(this.maxCharWidthPerRound.get(column)*this.charWidth,minimalVertexSize) + paddingBetweenVertices;
        }
        x += paddingBetweenVertices/2;
        x += Integer.max(this.maxCharWidthPerRound.get(coords.vertex_round)*this.charWidth,minimalVertexSize)/2;
        // ***
        int y = paddingToBorder/2;
        int peerIndex = agentsNames.indexOf(coords.proposer_node);
        for (int row = 0; row < peerIndex ; row ++) {
            String agentName = agentsNames.get(row);
            y += Integer.max(this.maxLineHeightPerNode.get(agentName)*this.lineHeight,minimalVertexSize) + paddingBetweenVertices;
        }
        y += paddingBetweenVertices/2;
        y += Integer.max(this.maxLineHeightPerNode.get(coords.proposer_node)*this.lineHeight,minimalVertexSize)/2;
        // ***
        return Pair.of(x,y);
    }

    public Pair<Integer,Integer> getVertexWidthAndHeight(DagVertexCoordinates coords) {
        return Pair.of(
                Integer.max(this.maxCharWidthPerRound.get(coords.vertex_round)*this.charWidth,minimalVertexSize),
                Integer.max(this.maxLineHeightPerNode.get(coords.proposer_node)*this.lineHeight,minimalVertexSize)
        );
    }

}


