/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.draw;


import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.env.DagRiderContext;
import max.model.ledger.dag_rider.env.DagRiderEnvironment;
import max.model.ledger.dag_rider.role.RDagRiderChannel;
import max.model.ledger.dag_rider.role.RDagRiderNode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.function.Function;


/**
 * Action executed by the DagRider environment to draw all the local DAGs of all the DagRider nodes
 * at a given snapshot in time.
 *
 * By drawing we mean generate PNG images of each DAG.
 *
 * @author Erwan Mahe
 */
public class AcDrawDagRiderDags<T_tx,T_st extends AbstractLocalLedgerState<T_tx>>
        extends Action<DagRiderEnvironment<T_tx,T_st>> {

    private final String pathPrefix;

    private final Function<T_tx, String> transactionPrinter;

    private final HashMap<String, ArrayList<String>> validatorNodesLabelsToDraw;

    private final Optional<String> singleNodeReference;

    public AcDrawDagRiderDags(String environment,
                              DagRiderEnvironment<T_tx,T_st> owner,
                              Function<T_tx, String> transactionPrinter,
                              String pathPrefix,
                              HashMap<String, ArrayList<String>> validatorNodesLabelsToDraw) {
        super(environment, RDagRiderChannel.class, owner);
        this.transactionPrinter = transactionPrinter;
        this.pathPrefix = pathPrefix;
        this.validatorNodesLabelsToDraw = validatorNodesLabelsToDraw;
        this.singleNodeReference = Optional.empty();
    }

    public AcDrawDagRiderDags(String environment,
                              DagRiderEnvironment<T_tx,T_st> owner,
                              Function<T_tx, String> transactionPrinter,
                              String pathPrefix,
                              HashMap<String, ArrayList<String>> validatorNodesLabelsToDraw,
                              Optional<String> singleNodeReference) {
        super(environment, RDagRiderChannel.class, owner);
        this.transactionPrinter = transactionPrinter;
        this.pathPrefix = pathPrefix;
        this.validatorNodesLabelsToDraw = validatorNodesLabelsToDraw;
        this.singleNodeReference = singleNodeReference;
    }

    @Override
    public void execute() {
        for (AgentAddress address : this.getOwner().getAgentsWithRole(this.getEnvironment(), RDagRiderNode.class)) {
            DagRiderAgent agent = (DagRiderAgent) address.getAgent();
            boolean draw;
            if (this.singleNodeReference.isPresent()) {
                if (agent.getName().equals(this.singleNodeReference.get())) {
                    draw = true;
                } else {
                    draw = false;
                }
            } else {
                draw = true;
            }
            if (draw) {
                DagRiderContext<T_tx,T_st> context = (DagRiderContext<T_tx,T_st>) agent.getContext(this.getEnvironment());
                DagRiderGraphDrawer.drawDrGraph(context,agent.getName(),this.transactionPrinter, this.pathPrefix, this.validatorNodesLabelsToDraw);
            }
        }
    }

    @Override
    public <T extends Action<DagRiderEnvironment<T_tx,T_st>>> T copy() {
        return (T) new AcDrawDagRiderDags<T_tx,T_st>(
                getEnvironment(),
                getOwner(),
                this.transactionPrinter,
                this.pathPrefix,
                this.validatorNodesLabelsToDraw,
                this.singleNodeReference
        );
    }
}