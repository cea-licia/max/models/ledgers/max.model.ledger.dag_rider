/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.role;


import max.model.ledger.abstract_ledger.role.RAbstractLedgerChannel;

/**
 * Role taken by the DagRider environment.
 *
 * @author Erwan Mahe
 */
public interface RDagRiderChannel extends RAbstractLedgerChannel {}
