/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.usecase.puzzle.action;


import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import max.model.ledger.abstract_ledger.action.transact.AcReceiveTransactionToPutInMempool;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupLocalLedgerState;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransactionGenerator;
import max.model.ledger.dag_rider.action.protocol.AcAttemptDagRiderProgress;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.env.DagRiderEnvironment;
import max.model.ledger.dag_rider.role.RDagRiderChannel;
import max.model.ledger.dag_rider.role.RDagRiderNode;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.*;


/**
 * Action executed by the environment to stimulate progress on each DagRider node.
 *
 * @author Erwan Mahe
 */
public class AcAddHeartbeatTransactionAndProgressNodes extends Action<DagRiderEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>> {

    // transmission delay from clients to nodes
    private final Optional<DelaySpecification> randomNoiseDelaySpecification;

    private final boolean addHeartbeat;

    public AcAddHeartbeatTransactionAndProgressNodes(String environment,
                                                     DagRiderEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState> owner,
                                                     Optional<DelaySpecification> randomNoiseDelaySpecification,
                                                     boolean addHeartbeat) {
        super(environment, RDagRiderChannel.class, owner);
        this.randomNoiseDelaySpecification = randomNoiseDelaySpecification;
        this.addHeartbeat = addHeartbeat;
    }


    @Override
    public void execute() {
        List<AgentAddress> dagRiderNodesAddresses = new ArrayList<>(this.getOwner().getAgentsWithRole(this.getEnvironment(), RDagRiderNode.class));
        Collections.shuffle(dagRiderNodesAddresses, new SecureRandom());
        Iterator<AgentAddress> it = dagRiderNodesAddresses.iterator();
        while(it.hasNext()) {
            // get node
            AgentAddress nodeAddress = it.next();
            DagRiderAgent nodeAgent = (DagRiderAgent) nodeAddress.getAgent();
            // get delay to stimulate
            int delay;
            if (this.randomNoiseDelaySpecification.isPresent()) {
                delay = this.randomNoiseDelaySpecification.get().get_concrete_delay();
            } else {
                delay = 0;
            }
            //
            final var nextTime = getOwner().getSimulationTime()
                    .getCurrentTick().add(BigDecimal.valueOf(delay));
            // if add_heartbeat we add a transaction
            if (this.addHeartbeat) {
                // create transaction
                PuzzleMockupTransaction tx = PuzzleMockupTransactionGenerator.getInstance().createNewHeartbeatTransaction();
                // ***
                Action<?> receive_hearbeat_action = new AcReceiveTransactionToPutInMempool<PuzzleMockupTransaction, PuzzleMockupLocalLedgerState,DagRiderAgent>(
                        this.getEnvironment(),
                        nodeAgent,
                        tx);
                getOwner().schedule(
                        new ActionActivator<>(
                                ActivationScheduleFactory.createOneTime(nextTime),
                                receive_hearbeat_action
                        ));
            }
            // we stimulate the node to attempt to progress
            Action<?> progress_dag_action = new AcAttemptDagRiderProgress<>(this.getEnvironment(), nodeAgent);
            getOwner().schedule(
                    new ActionActivator<>(
                            ActivationScheduleFactory.createOneTime(nextTime),
                            progress_dag_action
                    ));
        }
    }

    @Override
    public <T extends Action<DagRiderEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>>> T copy() {
        return (T) new AcAddHeartbeatTransactionAndProgressNodes(
                getEnvironment(),
                getOwner(),
                this.randomNoiseDelaySpecification,
                this.addHeartbeat);
    }

}
