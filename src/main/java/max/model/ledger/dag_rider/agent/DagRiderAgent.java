/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.agent;

import max.core.action.Plan;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.ledger.dag_rider.role.RDagRiderNode;

/**
 * Base class for DagRider agents.
 *
 * @author Erwan Mahe
 */
public class DagRiderAgent extends BroadcastPeerAgent {

    public DagRiderAgent(Plan<? extends DagRiderAgent> plan) {
        super(plan);
        addPlayableRole(RDagRiderNode.class);
    }

}
