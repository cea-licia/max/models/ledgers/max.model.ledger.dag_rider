/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.env;


import java.util.*;
import java.util.stream.Stream;

import max.model.ledger.abstract_ledger.env.LedgerValidatorContext;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.behavior.graph.DagRiderGraph;
import max.model.ledger.dag_rider.behavior.graph.DagVertexCoordinates;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.inwave_deterministic_order.AbstractInWaveDeterministicOrder;
import max.model.ledger.dag_rider.behavior.proposer.DagRiderVertexProposer;


/**
 * Context used in the {@link DagRiderEnvironment} and attributed to DagRider nodes.
 *
 * Serves as local storage for DagRider nodes.
 *
 * @param <T_tx> Transaction type
 *
 * @author Erwan Mahe
 */
public class DagRiderContext<T_tx, T_st extends AbstractLocalLedgerState<T_tx>> extends LedgerValidatorContext<T_tx,T_st> {

    /**
     * Message storage
     * Contains vertex proposals that have been received from other DagRiderNodes
     * but not yet inserted into the DAG
     * */
    private final Set<DagVertex<T_tx>> received_proposals;

    /**
     * The DagRider DAG data structure
     * It only contains information about which vertices are in the DAG and the strong and weak edges between them
     * Information about the division of the DAG into waves is not here
     * */
    private final DagRiderGraph<T_tx> graph;


    /**
     * Defines the behavior by which the node proposes new vertices.
     * */
    public DagRiderVertexProposer<T_tx> vertexProposer;

    /**
     * the value that is used (locally, on this DagRider node)
     * to determine required numbers of strong edges w.r.t. the number of nodes in the network
     * **/
    public int byzantineThresholdF;

    /**
     * The deterministic order that is used to order vertices part of the same wave.
     * **/
    public AbstractInWaveDeterministicOrder<T_tx> deterministicOrder;


    /**
     * Contains information about the waves that have been processed.
     * For each delivered vertex, gives the wave as part of which it has been delivered
     * **/
    public final HashMap<DagVertexCoordinates,Integer> wave_map;


    /**
     * Current round from the perspective of the Node
     * i.e. round of the last vertex which it has added
     * **/
    private int current_round;


    /**
     * Decided Wave from the perspective of the Node
     * i.e. the last wave that was processed and which contained vertices ordered
     * **/
    private int decided_wave;

    /**
     * Default constructor.
     */
    public DagRiderContext(DagRiderAgent owner,
                           DagRiderEnvironment<T_tx,T_st> drEnvironment) {
        super(owner, drEnvironment);

        this.received_proposals = new HashSet<>();
        this.graph = new DagRiderGraph<T_tx>();
        this.wave_map = new HashMap<>();
        this.current_round = 0;
        this.decided_wave = 0;
    }

    public void initialize_dag(Set<String> proposer_nodes) {
        this.graph.initialize_dag(proposer_nodes);
    }

    public Set<DagVertex<T_tx>> get_DR_received_proposals() {
        return this.received_proposals;
    }

    public DagRiderGraph<T_tx> get_DR_graph() {
        return this.graph;
    }

    /*public HashMap<String,Integer> get_DR_wave() {
        return this.wave_frontier;
    }*/

    public int get_DR_current_round() {
        return current_round;
    }

    public int get_decided_wave() {
        return this.decided_wave;
    }

    public void set_decided_wave(int wave_number) {
        this.decided_wave = wave_number;
    }

    public void increment_DR_current_round() {
        this.current_round = this.current_round + 1;
    }


    public List<DagVertexCoordinates> get_leader_stack_from_wave(int wave_number) {
        assert(wave_number > this.decided_wave);
        List<DagVertexCoordinates> leader_stack = new ArrayList<>();
        DagVertexCoordinates leader_coords = this.graph.get_wave_vertex_leader_coordinates(wave_number);
        if (!this.graph.contains_vertex_at_position(leader_coords)) {
            getOwner().getLogger().info("DAG does not contain wave leader of wave " + wave_number);
            return new ArrayList<>();
        }
        // ***
        int leader_determination_round = (4*(wave_number-1) + 4);
        getOwner().getLogger().info("counting strong paths between leader vertex : " + leader_coords + " and round : " + leader_determination_round);
        final int strong_paths_count = this.graph.count_strong_paths_between_vertex_and_round_number(leader_coords,leader_determination_round);
        getOwner().getLogger().info("there are " + strong_paths_count + " strong paths");
        // ***
        if ( strong_paths_count < (2*this.byzantineThresholdF) + 1 ) {
            getOwner().getLogger().info("DAG does not contain enough strong paths from round 4 of wave to leader in round 1 of wave " + wave_number);
            return new ArrayList<>();
        }
        leader_stack.add(leader_coords);
        for (int w = wave_number - 1; w > this.decided_wave  ; w --) {
            DagVertexCoordinates w_lead_coords = this.graph.get_wave_vertex_leader_coordinates(w);
            if (this.graph.has_path(false,leader_coords,w_lead_coords)) {
                leader_stack.add(w_lead_coords);
                leader_coords = w_lead_coords;
                getOwner().getLogger().info("including leader of wave " + w);
            } else {
                getOwner().getLogger().info("skipping leader of wave " + w);
            }
        }
        return leader_stack;
    }




    public void order_and_deliver_vertices(List<DagVertexCoordinates> leader_stack) {
        getOwner().getLogger().info("processing leader stack containing " + leader_stack.size() + " leaders");
        while (!leader_stack.isEmpty()) {
            DagVertexCoordinates leader_coord = leader_stack.remove(leader_stack.size() - 1);
            int wave_number = 1 + (leader_coord.vertex_round / 4);
            // ***
            getOwner().getLogger().info("processing leader of wave " + wave_number + " at position " + leader_coord);
            // vertices that will be delivered with current wave, ranked by round
            HashMap<Integer, List<DagVertex<T_tx>>> verticesToDeliverRankedByRound = new HashMap<>();
            // queue to explore causal sub-graph starting from wave leader vertex
            List<DagVertex<T_tx>> to_add_queue = new ArrayList<>();
            to_add_queue.add(this.graph.get_vertex_at_position(leader_coord));
            while (!to_add_queue.isEmpty()) {
                DagVertex<T_tx> to_add = to_add_queue.remove(to_add_queue.size() - 1);
                // adding explored vertex to vertices_to_deliver
                verticesToDeliverRankedByRound
                        .computeIfAbsent(to_add.coordinates.vertex_round, k -> new ArrayList<>())
                        .add(to_add);
                // updating new wave frontier
                if (!this.wave_map.containsKey(to_add.coordinates)) {
                    this.wave_map.put(to_add.coordinates, wave_number);
                }
                getOwner().getLogger().info("adding vertex to deliver " + to_add);
                // considering targets of edges if not already behind wave frontier or in verticesToDeliverRankedByRound
                Stream.concat(to_add.strong_edges.stream(),to_add.weak_edges.stream()).forEach(
                        vert_ref -> {
                            DagVertex<T_tx> target = this.graph.get_vertex_at_position(vert_ref.coordinates);
                            if (!this.wave_map.containsKey(vert_ref.coordinates)) {
                                boolean addToQueue = true;
                                if (verticesToDeliverRankedByRound.containsKey(target.coordinates.vertex_round)) {
                                    if (verticesToDeliverRankedByRound.get(target.coordinates.vertex_round).contains(target)) {
                                        addToQueue = false;
                                    }
                                }
                                if (addToQueue) {
                                    if (!to_add_queue.contains(target)) {
                                        to_add_queue.add(target);
                                    }
                                }
                            }
                        }
                );
            }

            ArrayList<DagVertex<T_tx>> ordered_vertices_to_deliver =  this.deterministicOrder.order_vertices(
                    this.get_DR_graph(),
                    verticesToDeliverRankedByRound,
                    wave_number
            );
            // delivering vertices
            List<T_tx> waveTransactions = new ArrayList<>();
            for (DagVertex<T_tx> vertex : ordered_vertices_to_deliver) {
                getOwner().getLogger().info(
                        "delivering vertex at coordinates " + vertex.coordinates +
                                " with " + vertex.transactions.size() + " transactions " +
                        "as part of wave " + wave_number);
                waveTransactions.addAll(vertex.transactions);
            }
            this.ledgerValidatorState.onLedgerDeliverBlock(waveTransactions, getOwner().getLogger());
        }
    }

}

