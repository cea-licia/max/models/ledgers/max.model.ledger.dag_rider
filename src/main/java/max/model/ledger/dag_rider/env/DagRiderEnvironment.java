/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.env;

import max.core.Context;
import max.core.agent.SimulatedAgent;

import max.model.ledger.abstract_ledger.env.AbstractLedgerEnvironment;
import max.model.ledger.abstract_ledger.state.AbstractLocalLedgerState;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.role.RDagRiderNode;

import java.util.logging.Level;

/**
 * A minimal DagRider Environment with a single possible role : that of {@link RDagRiderNode}
 *
 * @see DagRiderContext
 * @param <T_tx> Transaction type
 *
 *
 * @author Erwan Mahe
 */
public class DagRiderEnvironment<T_tx,
        T_st extends AbstractLocalLedgerState<T_tx>
        > extends AbstractLedgerEnvironment<T_tx,T_st> {

    public String reliableBroadcastEnvironmentName;

    public DagRiderEnvironment() {
        super();
        getLogger().setLevel(Level.INFO);
        addAllowedRole(RDagRiderNode.class);
    }

    @Override
    protected Context createContext(SimulatedAgent agent) {
        return new DagRiderContext<T_tx,T_st>((DagRiderAgent) agent, this);
    }

}
