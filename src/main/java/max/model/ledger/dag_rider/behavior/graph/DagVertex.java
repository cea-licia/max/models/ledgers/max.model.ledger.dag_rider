/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/



package max.model.ledger.dag_rider.behavior.graph;

import java.util.List;
import java.util.Optional;

import madkit.kernel.AgentLogger;
import max.model.ledger.dag_rider.error.MalformedDagVertexProposalException;
import org.apache.commons.lang3.tuple.Pair;


/**
 * To be used as message payload for DagRider propotocol messages
 * It describes a proposal for a new vertex to add to the DAG.
 *
 * These vertices constitute the DAG (Directed Acyclic Graph) at the basis
 * of the DAG-Rider protocol
 *
 * @param <T_tx> Type of transactions that are to be ordered by Dag-Rider
 *
 * It contains information about the vertex's content
 * via {@link DagVertex#transactions} attribute
 *
 * And the vertex's expected position in the grid structure of DAg Rider's DAG via
 * the {@link DagVertex#coordinates} attribute
 *
 * @author Erwan Mahe
 * **/
public class DagVertex<T_tx> {

    /**
     * The list of transactions that is inserted in this vertex of the DAG
     * "T_tx" refers to the type of transactions.
     * **/
    public final List<T_tx> transactions;

    public final DagVertexCoordinates coordinates;

    public final List<DagVertexReference> strong_edges;

    public final List<DagVertexReference> weak_edges;

    /**
     * Default constructor.
     *
     * @param transactions content of the vertex (a list of transactions to be added to the DAG)
     * @param coordinates the expected position of the proposal in the DAG grid structure
     * @param strong_edges the edges towards vertices of the previous column
     * @param weak_edges the edges towards vertices of columns before the previous column
     * */
    public DagVertex(List<T_tx> transactions,
                     DagVertexCoordinates coordinates,
                     List<DagVertexReference> strong_edges,
                     List<DagVertexReference> weak_edges) {
        this.transactions = transactions;
        this.coordinates = coordinates;
        this.strong_edges = strong_edges;
        this.weak_edges = weak_edges;
    }

    public void check_edges(Optional<AgentLogger> opt_logger, int byzantineThresholdF) throws MalformedDagVertexProposalException {

        // *** CHECKING STRONG EDGES
        final int min_strong_edges = (2*byzantineThresholdF) + 1;
        if (strong_edges.size() < min_strong_edges) {
            throw new MalformedDagVertexProposalException("Dag Vertex Proposal has " + strong_edges.size() + " strong edges, which is less than the minimum of " + min_strong_edges);
        }
        for (DagVertexReference vert_ref : strong_edges) {
            if (vert_ref.coordinates.vertex_round != this.coordinates.vertex_round - 1) {
                throw new MalformedDagVertexProposalException("Strong edges must target vertices of previous round");
            }
        }
        // *** CHECKING WEAK EDGES
        if (weak_edges.size() > byzantineThresholdF) {
            // in the Byzantine Reliable Broadcast, the delivery operation of the broadcast might come very late
            // as a result, a node may submit several successive vertices which do not have strong edges towards each other
            // this may in turn require having many weak edges pointing towards the same row at different column numbers to catch up
            //throw new MalformedDagVertexProposalException("Dag Vertex Proposal has " + weak_edges.size() + " weak edges, which is more than the maximum of " + byzantineThresholdF);
            opt_logger.ifPresent(l -> l.warning("Dag Vertex Proposal has " + weak_edges.size() + " weak edges, which is more than the maximum of " + byzantineThresholdF));
        }
        for (DagVertexReference vert_ref : weak_edges) {
            if (vert_ref.coordinates.vertex_round >= this.coordinates.vertex_round - 1) {
                throw new MalformedDagVertexProposalException("Weak edges must target vertices strictly before the previous round");
            }
        }
    }

    @Override
    public int hashCode() {
        return Pair.of(
                Pair.of(
                        this.coordinates,
                        this.transactions
                ),
                Pair.of(
                        this.strong_edges,
                        this.weak_edges
                )
        ).hashCode();
    }

}
