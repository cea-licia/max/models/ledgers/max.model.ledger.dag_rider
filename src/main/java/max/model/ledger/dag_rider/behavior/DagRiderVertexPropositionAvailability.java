/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/


package max.model.ledger.dag_rider.behavior;


/**
 * Different situations that may occur whenever we want to
 * create a new vertex at a specific position that will then be
 * inserted to the DAG
 *
 * @author Erwan Mahe
 * **/
public enum DagRiderVertexPropositionAvailability {
    CanProposeNewVertex,
    UninitializedDAG,
    RoundIsZeroOrLess,
    NotEnoughPossibleStrongEdges,
}
