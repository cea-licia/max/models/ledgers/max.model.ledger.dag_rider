/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.proposer;


import max.model.ledger.dag_rider.behavior.graph.DagRiderGraph;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.graph.DagVertexCoordinates;
import max.model.ledger.dag_rider.behavior.hash.DrAbstractHasherForParentSecuring;

import java.util.List;

/**
 * Interface to define the behavior by which a node proposes a new vertex.
 *
 * @author Erwan Mahe
 * **/
public interface DagRiderVertexProposer<T_tx> {


    boolean stall_proposal_depending_on_txs_extracted_from_mempool(List<T_tx> transactions);

    int count_vertices_at_round(int round_number, DagRiderGraph<T_tx> graph);

    DagVertex<T_tx> make_new_proposal(
            DagVertexCoordinates coordsAtWhichToInsertProposal,
            List<T_tx> transactions,
            DrAbstractHasherForParentSecuring<T_tx> hasher,
            DagRiderGraph<T_tx> graph,
            int byzantineThresholdF);

}
