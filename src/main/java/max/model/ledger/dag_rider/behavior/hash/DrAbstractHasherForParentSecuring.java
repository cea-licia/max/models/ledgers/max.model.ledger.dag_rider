/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.hash;

import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.graph.DagVertexReference;


/**
 * A hasher that specifies how transactions and vertices of the DAG are hashed
 *
 * @author Erwan Mahe
 */
public abstract class DrAbstractHasherForParentSecuring<T_tx> {

    public abstract int make_transaction_hash(T_tx tx);

    public int make_vertex_reference_hash(DagVertexReference vert_ref) {
        int coord_hash = vert_ref.coordinates.hashCode();
        return coord_hash*vert_ref.target_hash_value;
    }

    public int make_vertex_hash(DagVertex<T_tx> vert) {
        int trans_hash = 0;
        for (T_tx tr : vert.transactions) {
            trans_hash += this.make_transaction_hash(tr);
        }
        int coord_hash = vert.coordinates.hashCode();
        int edge_hash = 0;
        for (DagVertexReference vert_ref : vert.strong_edges) {
            edge_hash += 3*this.make_vertex_reference_hash(vert_ref);
        }
        for (DagVertexReference vert_ref : vert.weak_edges) {
            edge_hash += 5*this.make_vertex_reference_hash(vert_ref);
        }
        return trans_hash + coord_hash + edge_hash;
    }

}
