/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.inwave_deterministic_order;


import max.model.ledger.dag_rider.behavior.graph.DagRiderGraph;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.*;

/**
 * The deterministic order between vertices of different nodes at each round must be set
 * it must be the same for every node
 * We implement it as follows :
 * - we randomly shuffle the set of vertices
 *
 * @author Erwan Mahe
 * **/
public class FullShuffleInWaveDeterministicOrder<T_tx> implements AbstractInWaveDeterministicOrder<T_tx> {

    @Override
    public ArrayList<DagVertex<T_tx>> order_vertices(
            DagRiderGraph<T_tx> graph,
            HashMap<Integer, List<DagVertex<T_tx>>> verticesToDeliverRankedByRound,
            int wave_number
    ) {

        ArrayList<DagVertex<T_tx>> toDeliver = new ArrayList<>();
        verticesToDeliverRankedByRound.values().forEach(toDeliver::addAll);

        // we shuffle using the wave number as seed
        SecureRandom randomShuffler;
        try {
            randomShuffler = SecureRandom.getInstance("SHA1PRNG", "SUN");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException(e);
        }
        randomShuffler.setSeed(10L * wave_number);
        Collections.shuffle(toDeliver, randomShuffler);

        return toDeliver;
    }

}
