/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.inwave_deterministic_order;


import max.model.ledger.dag_rider.behavior.graph.DagRiderGraph;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * In Dag-Rider, the order with which vertices that are part of the same wave are finalized depends on a certain deterministic order.
 * In the original paper, this order is not specified. Its only requirement is that it is deterministic so as to preserve consistency
 * i.e., every node that order the same wave locally will obtain the same ordering of vertices (and thus transactions).
 *
 * This interface let the modeller define its own deterministic order (or use one of the default implementations).
 *
 * @author Erwan Mahe
 * **/
public interface AbstractInWaveDeterministicOrder<T_tx> {

    ArrayList<DagVertex<T_tx>> order_vertices(DagRiderGraph<T_tx> graph,
                                              HashMap<Integer, List<DagVertex<T_tx>>> verticesToDeliverRankedByRound,
                                              int waveNumber);

}
