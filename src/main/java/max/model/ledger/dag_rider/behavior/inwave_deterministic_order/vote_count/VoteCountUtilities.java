/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.inwave_deterministic_order.vote_count;

import max.model.ledger.dag_rider.behavior.graph.DagRiderGraph;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.graph.DagVertexCoordinates;
import org.jgrapht.alg.connectivity.KosarajuStrongConnectivityInspector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;


import java.util.*;
import java.util.stream.Collectors;

/**
 * Utilities for the wave vertex ordering scheme based on keeping track of the order of votes.
 *
 * @author Erwan Mahe
 * **/
public class VoteCountUtilities {


    /**
     * Computes the vote table for a subgraph of the input *graph* that corresponds
     * to the vertices in the input *verticesToDeliverRankedByRound*
     *
     * The vote table stores a map of (vertex Id: voteRecord)
     * Each vote record is a hash map between (nodeId: round) that records the
     * earliest round number that a node voted for the vertex
     *
     * @param <T_tx> : the type of the transactions in the DAG vertices
     * @param graph : the DAG
     * @param verticesToDeliverRankedByRound : the vertices of the wave that is considered, ranked by round
     **/
    public static <T_tx> HashMap<DagVertexCoordinates, HashMap<String,Integer>> compute_vote_table(
            DagRiderGraph<T_tx> graph,
            HashMap<Integer, List<DagVertex<T_tx>>> verticesToDeliverRankedByRound
    ) {
        HashMap<DagVertexCoordinates, HashMap<String,Integer>> verticesBoard = new HashMap<>();
        // we start by the smallest round
        List<Integer> rounds = verticesToDeliverRankedByRound.keySet().stream().sorted().collect(Collectors.toList());
        for (int roundIndex = 0; roundIndex < rounds.size(); roundIndex++) {
            Integer round = rounds.get(roundIndex);
            for (DagVertex<T_tx> vertex : verticesToDeliverRankedByRound.get(round)) {
                {
                    // a node votes for its own vertex proposal at the corresponding round
                    HashMap<String,Integer> voteForSelf = new HashMap<>();
                    voteForSelf.put(vertex.coordinates.proposer_node,round);
                    verticesBoard.put(vertex.coordinates, voteForSelf);
                }
                // for all vertices of lower rounds *prevRound*
                for (int prevRoundIndex = 0; prevRoundIndex < roundIndex; prevRoundIndex++) {
                    Integer prevRound = rounds.get(prevRoundIndex);
                    for (DagVertex<T_tx> prevVertex : verticesToDeliverRankedByRound.get(prevRound)) {
                        HashMap<String,Integer> votesForRef = verticesBoard.get(prevVertex.coordinates);
                        if (!votesForRef.containsKey(vertex.coordinates.proposer_node)) {
                            // this means the node has not yet voted for that vertex at an earlier round
                            // the node votes for it at round *round* if there is a path between the new vertex and the target
                            if (graph.has_path(true,vertex.coordinates, prevVertex.coordinates)) {
                                votesForRef.put(vertex.coordinates.proposer_node,round);
                                verticesBoard.put(prevVertex.coordinates, votesForRef);
                            }
                        }
                    }
                }
            }
        }
        return verticesBoard;
    }

    /**
     * We compare vertices v1 and v2 as follows :
     *  considering "v1First" : the number of nodes that voted for v1 first before they voted for v2
     *  and         "v2First" : the number of nodes that voted for v2 first before they voted for v1
     * We consider that v1 is "smaller" than v2 i.e. should come earlier iff:
     *  v1First IS GREATER THAN v2First PLUS threshold
     * Where "threshold" is an input parameter
     *
     * We may then use this compare relation to define a partial order between all the vertices in the wave
     *
     * @param <T_tx> : the type of the transactions in the DAG vertices
     * @param graph : the DAG
     * @param verticesBoard : the vote table
     * @param threshold : the threshold on the difference in the number of earlier votes
     * **/
    public static <T_tx> Comparator<DagVertex<T_tx>>  get_vertex_comparator_from_vote_table(
            DagRiderGraph<T_tx> graph,
            HashMap<DagVertexCoordinates, HashMap<String,Integer>> verticesBoard,
            int threshold
    ) {
        return new Comparator<DagVertex<T_tx>>() {
            @Override
            public int compare(DagVertex<T_tx> v1, DagVertex<T_tx> v2) {
                HashMap<String,Integer> board1 = verticesBoard.get(v1.coordinates);
                HashMap<String,Integer> board2 = verticesBoard.get(v2.coordinates);
                // if a majority of nodes vote for v1 first then v1 is lower
                // if a majority of nodes vote for v2 first then v1 is higher
                // otherwise they are equivalent
                int v1First = 0;
                int v2First = 0;
                for (String proposerNode : graph.proposer_nodes) {
                    if (board1.containsKey(proposerNode) && board2.containsKey(proposerNode)) {
                        Integer roundPropVotedForV1 = board1.get(proposerNode);
                        Integer roundPropVotedForV2 = board2.get(proposerNode);
                        if (roundPropVotedForV1 < roundPropVotedForV2) {
                            v1First += 1;
                        }
                        if (roundPropVotedForV2 < roundPropVotedForV1) {
                            v2First += 1;
                        }
                    }
                }
                /*
                * The compare() method in Java compares two class specific objects (x, y) given as parameters. It returns the value:
                    0  : if (x==y)  i.e. here (v1 == v2)
                    -1 : if (x < y) i.e. here (v1 < v2)
                    1  : if (x > y) i.e. here (v1 > v2)
                * */
                if (v1First > v2First + threshold) {
                    return -1;
                }
                if (v2First > v1First + threshold) {
                    return 1;
                }
                return 0;
            }
        };
    }

    /**
     * The precedence relation between vertices based on counting the number of earlier votes
     * is here put in the form of a directed graph.
     *
     * As there can be Condorcet cycles:
     * i.e., for three vertices v1 v2 and v3:
     * - a majority of nodes vote for v1 before they do so for v2
     * - a majority of nodes vote for v2 before they do so for v3
     * - a majority of nodes vote for v3 before they do so for v1
     * this graph may be cyclic.
     *
     * @param <T_tx> : the type of the transactions in the DAG vertices
     * @param verticesToDeliverRankedByRound : the vertices of the wave that is considered, ranked by round
     * @param comparator : the precedence relation on vertices
     * **/
    public static <T_tx> DefaultDirectedGraph<DagVertex<T_tx>, DefaultEdge> get_relation_as_graph_from_comparator(
            HashMap<Integer, List<DagVertex<T_tx>>> verticesToDeliverRankedByRound,
            Comparator<DagVertex<T_tx>> comparator
    ) {
        // put all the vertices of the wave in the same list
        ArrayList<DagVertex<T_tx>> allVerticesToDeliver = new ArrayList<>();
        verticesToDeliverRankedByRound.values().forEach(allVerticesToDeliver::addAll);

        // we represent the partial order between the vertices as a directed graph.
        DefaultDirectedGraph<DagVertex<T_tx>, DefaultEdge> relation_as_graph = new DefaultDirectedGraph(DefaultEdge.class);

        for (DagVertex<T_tx> vertex : allVerticesToDeliver) {
            relation_as_graph.addVertex(vertex);
        }
        for (int vertIndex = 0; vertIndex < allVerticesToDeliver.size(); vertIndex++) {
            DagVertex<T_tx> vertex = allVerticesToDeliver.get(vertIndex);
            for (int otherVertIndex = vertIndex + 1; otherVertIndex < allVerticesToDeliver.size(); otherVertIndex++) {
                DagVertex<T_tx> otherVertex = allVerticesToDeliver.get(otherVertIndex);
                // for all the other vertices
                // we might add an edge from "otherVertex" to "vertex"
                // if, according to the comparator, "vertex < otherVertex"
                var compared = comparator.compare(vertex,otherVertex);
                if (compared < 0) {
                    relation_as_graph.addEdge(otherVertex, vertex);
                }
                // and reciprocally, we might add an edge from "vertex" to "otherVertex"
                // if, according to the comparator, "otherVertex < vertex"
                if (compared > 0) {
                    relation_as_graph.addEdge(vertex, otherVertex);
                }
            }
        }

        return relation_as_graph;
    }

    /**
     * Returns the set of Sink Strongly Connected Components of a directed graph.
     *
     * @param <T_tx> : the type of the transactions in the DAG vertices
     * @param remaining_graph : what remains of the graph-representation of the precedence relation
     * **/
    public static <T_tx> Set<Set<DagVertex<T_tx>>> get_sink_strongly_connected_components(
            DefaultDirectedGraph<DagVertex<T_tx>, DefaultEdge> remaining_graph
    ) {
        var kosaraju = new KosarajuStrongConnectivityInspector<DagVertex<T_tx>,DefaultEdge>(remaining_graph);
        Set<Set<DagVertex<T_tx>>> sinks = new HashSet<>();
        // we iter the SCCs
        for (Set<DagVertex<T_tx>> scc : kosaraju.stronglyConnectedSets()) {
            // if the SCC is a "sink SCC"
            boolean isSink = true;
            iter_scc : for (DagVertex<T_tx> vertexInSCC : scc) {
                for (DefaultEdge outgoingEdge : remaining_graph.outgoingEdgesOf(vertexInSCC)) {
                    if (!scc.contains(remaining_graph.getEdgeTarget(outgoingEdge))) {
                        isSink = false;
                        break iter_scc;
                    }
                }
            }
            if (isSink) {
                sinks.add(scc);
            }
        }
        return sinks;
    }

}
