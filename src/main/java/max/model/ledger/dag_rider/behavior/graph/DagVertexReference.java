/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.graph;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Reference pointing towards a vertex of the DAG
 *
 * To be used in order to define strong edges and weak edges in proposals of new vertices
 *
 * It contains the following information about the vertex :
 * - via the {@link DagVertexReference#coordinates} attribute the coordinates i.e. round and proposer
 * - via the {@link DagVertexReference#target_hash_value} attribute the hashcode of the proposal
 *
 * @author Erwan Mahe
 * **/
public class DagVertexReference {

    public final DagVertexCoordinates coordinates;

    public final int target_hash_value;

    public DagVertexReference(DagVertexCoordinates coordinates, int target_hash_value) {
        this.coordinates = coordinates;
        this.target_hash_value = target_hash_value;
    }

    public int getVertexRound() {
        return this.coordinates.vertex_round;
    }

    @Override
    public int hashCode() {
        return Pair.of(this.coordinates,this.target_hash_value).hashCode();
    }

}
