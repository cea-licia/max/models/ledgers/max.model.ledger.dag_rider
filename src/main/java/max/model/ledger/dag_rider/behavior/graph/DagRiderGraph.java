/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.graph;


import madkit.kernel.AgentLogger;
import max.model.ledger.dag_rider.behavior.DagRiderVertexAdditionPossibility;
import max.model.ledger.dag_rider.behavior.DagRiderVertexPropositionAvailability;
import max.model.ledger.dag_rider.behavior.hash.DrAbstractHasherForParentSecuring;
import max.model.ledger.dag_rider.behavior.proposer.DagRiderVertexProposer;
import max.model.ledger.dag_rider.error.MalformedDagVertexProposalException;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.*;


/**
 * The graph data structure for the Dag Rider implementation
 *
 * @author Erwan Mahe
 * **/
public class DagRiderGraph<T_tx> {

    private Optional<AgentLogger> opt_logger;

    private boolean is_initialized;

    public Set<String> proposer_nodes;


    public final Map<DagVertexCoordinates, DagVertex<T_tx>> vertices;

    /**
     * Keeps track of vertices towards which no other vertex point.
     * **/
    public final HashSet<DagVertexCoordinates> orphanVertices;

    /**
     * Constructs new empty DAG
     * **/
    public DagRiderGraph() {
        this.opt_logger = Optional.empty();
        this.is_initialized = false;
        this.vertices = new HashMap<DagVertexCoordinates, DagVertex<T_tx>>();
        this.orphanVertices = new HashSet<>();
    }

    public void setLogger(AgentLogger logger) {
        logger.info("set logger for graph");
        this.opt_logger = Optional.of(logger);
    }

    private void debug_log(String log_msg) {
        this.opt_logger.ifPresent(logger -> {
            logger.info(log_msg);
        });
    }

    public void initialize_dag(Set<String> proposer_nodes) {
        // asserts this has been called only once
        assert(!this.is_initialized);
        this.is_initialized = true;
        // ***
        this.proposer_nodes = proposer_nodes;
        for (String agentName : proposer_nodes) {
            DagVertexCoordinates coords = new DagVertexCoordinates(0,agentName);
            DagVertex<T_tx> genesis = new DagVertex<T_tx>(new ArrayList<>(),coords,new ArrayList<>(),new ArrayList<>());
            this.vertices.put(genesis.coordinates,genesis);
            this.orphanVertices.add(coords);
            this.debug_log("inserted genesis vertex at position : " + genesis.coordinates);
        }
    }

    public DagVertex<T_tx> get_vertex_at_position(DagVertexCoordinates coordinates) {
        return this.vertices.get(coordinates);
    }

    public boolean contains_vertex_at_position(DagVertexCoordinates coordinates) {
        // uncomment for debugging
        //this.debug_log("checking presence of vertex at position : " + coordinates);
        return this.vertices.containsKey(coordinates);
    }

    /**
     * Checks that all targets of a vertex proposal edges are already in the graph
     *
     * @param proposal : the vertex that may be added
     * @param hasher : the hasher used to define the edges
     * @param byzantineThresholdF : the byzantine fault tolerance threshold
     * **/
    public DagRiderVertexAdditionPossibility can_add_vertex(
            DagVertex<T_tx> proposal,
            DrAbstractHasherForParentSecuring<T_tx> hasher,
            int byzantineThresholdF
    ) {
        // ensures DAG is initialized
        if (!this.is_initialized) {
            return DagRiderVertexAdditionPossibility.UninitializedDAG;
        }
        // ensures proposal is from a known proposer
        if (!this.proposer_nodes.contains(proposal.coordinates.proposer_node)) {
            return DagRiderVertexAdditionPossibility.UnknownProposer;
        }
        // ensures proposal is well formed
        try {
            proposal.check_edges(this.opt_logger,byzantineThresholdF);
        } catch (MalformedDagVertexProposalException e) {
            return DagRiderVertexAdditionPossibility.MalformedVertex;
        }
        // ensures DAG does not already contain a vertex at the given position
        if (this.contains_vertex_at_position(proposal.coordinates)) {
            return DagRiderVertexAdditionPossibility.CoordinatesAlreadyOccupied;
        }
        // CHECKING STRONG EDGE targets are in the graph
        for (DagVertexReference vert_ref : proposal.strong_edges) {
            if (!this.vertices.containsKey(vert_ref.coordinates)) {
                return DagRiderVertexAdditionPossibility.MissingEdgeTarget;
            } else {
                DagVertex<T_tx> target = this.get_vertex_at_position(vert_ref.coordinates);
                if (hasher.make_vertex_hash(target) != vert_ref.target_hash_value) {
                    return DagRiderVertexAdditionPossibility.WrongHashCodeForEdgeTarget;
                }
            }
        }
        // CHECKING WEAK EDGE targets are in the graph
        for (DagVertexReference vert_ref : proposal.weak_edges) {
            if (!this.vertices.containsKey(vert_ref.coordinates)) {
                return DagRiderVertexAdditionPossibility.MissingEdgeTarget;
            } else {
                DagVertex<T_tx> target = this.get_vertex_at_position(vert_ref.coordinates);
                if (hasher.make_vertex_hash(target) != vert_ref.target_hash_value) {
                    return DagRiderVertexAdditionPossibility.WrongHashCodeForEdgeTarget;
                }
            }
        }
        return DagRiderVertexAdditionPossibility.CanAddToDAG;
    }

    /**
     * Inserts a new vertex proposal into the DAG
     *
     * @param proposal : the vertex to add
     * **/
    public void add_vertex(DagVertex<T_tx> proposal) {
        for (DagVertexReference ref : proposal.strong_edges) {
            this.orphanVertices.remove(ref.coordinates);
        }
        for (DagVertexReference ref : proposal.weak_edges) {
            this.orphanVertices.remove(ref.coordinates);
        }
        this.vertices.put(proposal.coordinates, proposal);
        this.orphanVertices.add(proposal.coordinates);
    }

    /**
     * Checks that there is a path between two coordinates in the DAG
     * @param includes_weak_edges : determines whether or not we should include weak edges in path (otherwise only strong edges)
     * @param coord1 : the coordinate of the origin of the path (vertex of higher round)
     * @param coord2 : the coordinate of the target of the path (vertex of lower round)
     * **/
    public boolean has_path(boolean includes_weak_edges,
                            DagVertexCoordinates coord1,
                            DagVertexCoordinates coord2) {
        if (!this.vertices.containsKey(coord1)) {
            return false;
        }
        if (!this.vertices.containsKey(coord2)) {
            return false;
        }
        if (coord1.vertex_round == coord2.vertex_round) {
            return false;
        } else if (coord1.vertex_round < coord2.vertex_round) {
            return this.has_path(includes_weak_edges,coord2,coord1);
        }
        // stores visited coordinates
        Set<DagVertexCoordinates> visited = new HashSet<>();

        // queues coordinates which visitation is pending
        Deque<DagVertexCoordinates> queue = new ArrayDeque<>();

        // Initializing queue
        queue.push(coord1);

        while (!queue.isEmpty()) {
            DagVertexCoordinates current = queue.pop();
            // Marks current as visited
            visited.add(current);
            // If target is reached returns true
            if (current.equals(coord2)) {
                return true;
            }
            // Retrieving vertex at current coordinates
            DagVertex<T_tx> vertex = this.get_vertex_at_position(current);
            // Traversing strong edges
            for (DagVertexReference vert_ref : vertex.strong_edges) {
                if (!visited.contains(vert_ref.coordinates)) {
                    queue.push(vert_ref.coordinates);
                }
            }
            if (includes_weak_edges) {
                // Traversing weak edges
                for (DagVertexReference vert_ref : vertex.weak_edges) {
                    if (!visited.contains(vert_ref.coordinates)) {
                        queue.push(vert_ref.coordinates);
                    }
                }
            }
        }

        // If coord2 has not been reached, there is no path
        return false;
    }


    public DagRiderVertexPropositionAvailability can_make_new_proposal(
            String proposer_node,
            int current_round,
            DagRiderVertexProposer<T_tx> vertexProposer,
            int byzantineThresholdF) {
        // ensures DAG is initialized
        if (!this.is_initialized) {
            return DagRiderVertexPropositionAvailability.UninitializedDAG;
        }
        // cannot make new proposal at round 0 or less
        if (current_round <= 0) {
            return DagRiderVertexPropositionAvailability.RoundIsZeroOrLess;
        }
        int previous_round = current_round-1;
        // counts the number of possible strong edges
        int num_strong_edges = vertexProposer.count_vertices_at_round(previous_round,this);
        // CHECKS there are at least 2*F+1 possible strong edges
        if (num_strong_edges < ((2*byzantineThresholdF) + 1)) {
            return DagRiderVertexPropositionAvailability.NotEnoughPossibleStrongEdges;
        }
        // ***
        return DagRiderVertexPropositionAvailability.CanProposeNewVertex;
    }

    public String[] get_agents_lexicographic() {
        int number_of_proposers = this.proposer_nodes.size();
        // converting set to array
        String sorted_proposer_nodes[] = new String[number_of_proposers];
        sorted_proposer_nodes = this.proposer_nodes.toArray(sorted_proposer_nodes);
        // sorting String array in Lexicographical Order.
        // Ignoring the case of string.
        Arrays.sort(sorted_proposer_nodes,
                String.CASE_INSENSITIVE_ORDER);
        return sorted_proposer_nodes;
    }

    public DagVertexCoordinates get_wave_vertex_leader_coordinates(int wave_num) {
        assert(wave_num > 0);
        // Get ROUND of leader of wave following 'round(w,1)' with 'round(w,k) = 4*(w-1) + k'
        int round_of_leader = 4*(wave_num-1) + 1;
        // sorted list of proposers
        String[] sorted_proposer_nodes = this.get_agents_lexicographic();
        // random coin abstraction seeded on the wave number so that it is deterministic
        SecureRandom random_coin;
        try {
            random_coin = SecureRandom.getInstance("SHA1PRNG", "SUN");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException(e);
        }
        random_coin.setSeed(wave_num);
        int index_of_leader = random_coin.nextInt(this.proposer_nodes.size());
        // ***
        String leader_address = sorted_proposer_nodes[index_of_leader];
        // ***
        DagVertexCoordinates leader_coordinates = new DagVertexCoordinates(round_of_leader,leader_address);
        return leader_coordinates;
    }

    public int count_strong_paths_between_vertex_and_round_number(DagVertexCoordinates vertex_coord, int round_number) {
        assert(vertex_coord.vertex_round != round_number);
        int count = 0;
        for (String agentName : this.proposer_nodes) {
            DagVertexCoordinates at_round_coord = new DagVertexCoordinates(round_number,agentName);
            if (this.has_path(false,vertex_coord,at_round_coord)) {
                count += 1;
            }
        }
        return count;
    }


    @Override
    public String toString() {
        //return "\n" + this.vertices.keySet();

        List<String> rows = new ArrayList<>();
        for (String agentName : this.get_agents_lexicographic()) {
            var row_str = "\t" + agentName + " ";
            var round = 0;
            while (true) {
                DagVertexCoordinates coords = new DagVertexCoordinates(round,agentName);
                if (this.contains_vertex_at_position(coords)) {
                    row_str += " X";
                    round += 1;
                } else {
                    break;
                }
            }
            rows.add(row_str);
        }
        return "\n" + String.join("\n",rows);
    }

}
