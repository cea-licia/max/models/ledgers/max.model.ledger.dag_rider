/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.inwave_deterministic_order;


import max.model.ledger.dag_rider.behavior.graph.DagRiderGraph;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.*;

/**
 * The deterministic order between vertices of different nodes at each round must be set
 * it must be the same for every node
 * We implement it as follows :
 * - we rank vertices into distinct lists by the round at which they occur
 * - for each rank we shuffle the vertices at that rank randomly using the round number as a seed
 * - we then add vertices sequentially from the lowest to the highest round
 *
 * @author Erwan Mahe
 * **/
public class PerColumnShuffleInWaveDeterministicOrder<T_tx> implements AbstractInWaveDeterministicOrder<T_tx> {

    @Override
    public ArrayList<DagVertex<T_tx>> order_vertices(
            DagRiderGraph<T_tx> graph,
            HashMap<Integer, List<DagVertex<T_tx>>> verticesToDeliverRankedByRound,
            int wave_number
    ) {

        // we fill up the sorted vertices round by round
        List<Integer> sortedRounds = new ArrayList(verticesToDeliverRankedByRound.keySet());
        Collections.sort(sortedRounds);
        ArrayList<DagVertex<T_tx>> sorted_vertices_to_deliver = new ArrayList<>();
        for (Integer round : sortedRounds) {
            ArrayList<String> sorted_nodes_at_round = new ArrayList<>(List.of(graph.get_agents_lexicographic()));
            // we shuffle using the round number as seed
            SecureRandom randomShuffler;
            try {
                randomShuffler = SecureRandom.getInstance("SHA1PRNG", "SUN");
            } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                throw new RuntimeException(e);
            }
            randomShuffler.setSeed(10L *round);
            Collections.shuffle(sorted_nodes_at_round, randomShuffler);
            // ***
            // we now sort the vertices using the order of nodes
            List<DagVertex<T_tx>> toDeliverAtRound = verticesToDeliverRankedByRound.get(round);
            Collections.sort(toDeliverAtRound, new Comparator<DagVertex<T_tx>>() {
                @Override
                public int compare(DagVertex<T_tx> v1, DagVertex<T_tx> v2) {
                    Integer idx1 = sorted_nodes_at_round.indexOf(v1.coordinates.proposer_node);
                    Integer idx2 = sorted_nodes_at_round.indexOf(v2.coordinates.proposer_node);
                    return Integer.compare(idx1,idx2);
                }
            });
            // now that the vertices at that round are sorted, we add them to the list of vertices to deliver
            sorted_vertices_to_deliver.addAll(toDeliverAtRound);
        }

        return sorted_vertices_to_deliver;
    }

}
