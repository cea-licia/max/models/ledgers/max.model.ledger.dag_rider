/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.handler;


import max.model.broadcast.abstract_reliable_broadcast.behavior.ReliableBroadcastHandler;
import max.model.ledger.dag_rider.action.protocol.AcAddToDAG;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.env.DagRiderContext;

/**
 * Handler of delivery events for the Reliable Broadcast layer.
 * This will be executed whenever a new vertex proposal is delivered on a specific DagRider node
 * upon completion of its reliable broadcast.
 *
 * @author Erwan Mahe
 */
public class DagRiderVertexProposalOnBroadcastDeliveryHandler<T_tx> implements ReliableBroadcastHandler<DagVertex<T_tx>, DagRiderAgent> {

    @Override
    public void handle(DagVertex<T_tx> receivedVertexProposal, DagRiderAgent drAgent, String relevantEnvironment) {

        DagRiderContext<T_tx,?> DR_context = (DagRiderContext<T_tx,?>) drAgent.getContext(relevantEnvironment);

        // notifies application-side the reception of the transactions from DagRider's gossip via the reception of
        // the vertex and hence the transactions it contains
        /*drAgent.getLogger().info("acknowledging reception of transactions contained in the received vertex");
        for (T_tx tx : receivedVertexProposal.transactions) {
            DR_context.ledgerValidatorState.onLedgerReceiveTransaction(
                    tx,
                    TransactionReceptionNature.GOSSIP_FROM_PEERS,
                    drAgent.getLogger()
            );
        }*/
        drAgent.getLogger().info("purging local mempool from transactions that are in the received vertex (to mitigate duplicated transactions)");
        DR_context.clientTransactionsMempool.purgeMempoolOfDeliveredBlock(receivedVertexProposal.transactions);
        // add the received vertex to the buffered set of received vertices
        DR_context.get_DR_received_proposals().add(receivedVertexProposal);
        drAgent.getLogger().info("Buffered received Dag Rider vertex proposal from message handling :"
                + " from " + receivedVertexProposal.coordinates
                + " with " + receivedVertexProposal.transactions.size() + " transaction(s)"
                + ", " + receivedVertexProposal.strong_edges.size() + " strong edges"
                + " and " + receivedVertexProposal.weak_edges.size() + " weak edges"
        );
        // attempts to add vertices from the buffered set of received vertices to the local copy of the DAG
        new AcAddToDAG<>(relevantEnvironment,drAgent).execute();
    }

}
