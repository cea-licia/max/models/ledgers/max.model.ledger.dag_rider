/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.graph;


/**
 * Coordinates of a Vertex in a DagRider DAG
 *
 * Provides the vertex's expected position in the grid structure of DAg Rider's DAG via:
 * - {@link DagVertexCoordinates#vertex_round} attribute which gives its 'x' position (column)
 * - {@link DagVertexCoordinates#proposer_node} attribute which gives its 'y' position (row)
 *
 * @author Erwan Mahe
 * **/
public class DagVertexCoordinates {

    /**
     * Round at which this vertex is expected to be
     * It provides the 'x' position (column) of the vertex
     * in the DAG's grid structure (if this vertex is indeed added to the DAG)
     * **/
    public final int vertex_round;

    /**
     * Means to identify which Dag-Rider node has proposed this vertex
     *
     * It also provides the 'y' position (row) of the vertex
     * in the DAG's grid structure (if this vertex is indeed added to the DAG)
     * **/
    public final String proposer_node;

    public DagVertexCoordinates(int vertex_round, String proposer_node) {
        this.vertex_round = vertex_round;
        this.proposer_node = proposer_node;
    }

    @Override
    public String toString() {
        return "@(" + this.proposer_node + "|" + this.vertex_round + ")";
    }


    public String toSimpleString() {
        return this.proposer_node + "-" + this.vertex_round;
    }

    /**
     * Must Override to be used as key in a hashmap
     * **/
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        DagVertexCoordinates casted = (DagVertexCoordinates) o;
        if (casted.vertex_round != this.vertex_round) {
            return false;
        }
        if (casted.proposer_node != this.proposer_node) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return this.proposer_node.hashCode()*this.vertex_round;
    }

}
