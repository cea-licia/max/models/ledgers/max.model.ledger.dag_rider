/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.proposer;


import max.model.ledger.dag_rider.behavior.graph.DagRiderGraph;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.graph.DagVertexCoordinates;
import max.model.ledger.dag_rider.behavior.graph.DagVertexReference;
import max.model.ledger.dag_rider.behavior.hash.DrAbstractHasherForParentSecuring;

import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Interface to define the behavior by which a node proposes a new vertex.
 *
 * @author Erwan Mahe
 * **/
public class HonestDagVertexProposer<T_tx> implements DagRiderVertexProposer<T_tx> {

    WeakEdgesSelectionPolicy weakEdgesSelectionPolicy;

    boolean allowEmptyVertices;

    public HonestDagVertexProposer(boolean allowEmptyVertices,
                                   WeakEdgesSelectionPolicy weakEdgesSelectionPolicy) {
        this.allowEmptyVertices = allowEmptyVertices;
        this.weakEdgesSelectionPolicy = weakEdgesSelectionPolicy;
    }

    @Override
    public boolean stall_proposal_depending_on_txs_extracted_from_mempool(List<T_tx> transactions) {
        if(allowEmptyVertices) {
            return false;
        } else {
            // do not all
            return (transactions.isEmpty());
        }
    }

    @Override
    public int count_vertices_at_round(int round_number, DagRiderGraph<T_tx> graph) {
        int count = 0;
        for (String agentName : graph.proposer_nodes) {
            DagVertexCoordinates target_coord = new DagVertexCoordinates(round_number,agentName);
            if (graph.contains_vertex_at_position(target_coord)) {
                count += 1;
            }
        }
        return count;
    }

    @Override
    public DagVertex<T_tx> make_new_proposal(
            DagVertexCoordinates coordsAtWhichToInsertProposal,
            List<T_tx> transactions,
            DrAbstractHasherForParentSecuring<T_tx> hasher,
            DagRiderGraph<T_tx> graph,
            int byzantineThresholdF) {

        // ***
        int previous_round = coordsAtWhichToInsertProposal.vertex_round-1;
        // GATHER strong edges
        List<DagVertexReference> strong_edges = new ArrayList<>();
        for (String agentName : graph.proposer_nodes) {
            DagVertexCoordinates target_coord = new DagVertexCoordinates(previous_round,agentName);
            if (graph.contains_vertex_at_position(target_coord)) {
                DagVertex<T_tx> target = graph.get_vertex_at_position(target_coord);
                DagVertexReference vert_ref = new DagVertexReference(target_coord,hasher.make_vertex_hash(target));
                strong_edges.add(vert_ref);
            }
        }
        // Creates new proposal coordinates
        DagVertexCoordinates coords = new DagVertexCoordinates(coordsAtWhichToInsertProposal.vertex_round,coordsAtWhichToInsertProposal.proposer_node);
        // create intermediate vertex proposal (not yet with weak edges)
        DagVertex<T_tx> incomplete_proposal = new DagVertex<T_tx>(new ArrayList<T_tx>(),coords,strong_edges,new ArrayList<>());
        // GET weak edges
        List<DagVertexReference> weak_edges = this.get_weak_edges(incomplete_proposal,previous_round,hasher,graph,byzantineThresholdF);
        // create new proposal
        DagVertex<T_tx> new_proposal = new DagVertex<T_tx>(transactions,coords,strong_edges,weak_edges);
        // check edges (just to be sure)
        /*try {
            new_proposal.check_edges(byzantineThresholdF);
            return new_proposal;
        } catch (MalformedDagVertexProposalException e) {
            throw new RuntimeException(e);
        }*/
        return new_proposal;
    }

    private List<DagVertexReference> get_weak_edges(DagVertex<T_tx> incomplete_proposal,
                                                    int previous_round,
                                                    DrAbstractHasherForParentSecuring<T_tx> hasher,
                                                    DagRiderGraph<T_tx> graph,
                                                    int byzantineThresholdF) {
        // momentarily add the incomplete proposal
        graph.vertices.put(incomplete_proposal.coordinates, incomplete_proposal);
        List<DagVertexReference> weak_edges = new ArrayList<>();
        // add weak edges related to orphan vertices in the current local copy of the DAG
        for (DagVertexCoordinates orphan_coords : graph.orphanVertices) {
            if (orphan_coords.vertex_round <= previous_round) {
                // if there is no path (any, strong and/or weak) towards that orphan vertex add a weak edge
                if (!graph.has_path(true,incomplete_proposal.coordinates,orphan_coords)) {
                    DagVertex<T_tx> orphan_vertex = graph.get_vertex_at_position(orphan_coords);
                    DagVertexReference vert_ref = new DagVertexReference(orphan_coords,hasher.make_vertex_hash(orphan_vertex));
                    weak_edges.add(vert_ref);
                }
            }
        }
        // removes the incomplete proposal
        graph.vertices.remove(incomplete_proposal.coordinates);
        if (weak_edges.size() > byzantineThresholdF) {
            if (this.weakEdgesSelectionPolicy.equals(WeakEdgesSelectionPolicy.Retain_F_Random)) {
                Collections.shuffle(weak_edges, new SecureRandom());
                weak_edges = weak_edges.stream().limit(byzantineThresholdF).collect(Collectors.toList());
            } else if (this.weakEdgesSelectionPolicy.equals(WeakEdgesSelectionPolicy.Retain_F_Oldest)) {
                HashMap<Integer,List<DagVertexReference>> roundToEdges = new HashMap<>();
                for (DagVertexReference vert_ref : weak_edges) {
                    if (roundToEdges.containsKey(vert_ref.getVertexRound())) {
                        List<DagVertexReference> atRound = roundToEdges.get(vert_ref.getVertexRound());
                        atRound.add(vert_ref);
                        roundToEdges.put(vert_ref.getVertexRound(),atRound);
                    } else {
                        List<DagVertexReference> atRound = new ArrayList<>();
                        atRound.add(vert_ref);
                        roundToEdges.put(vert_ref.getVertexRound(),atRound);
                    }
                }
                List<Integer> sortedRounds = new ArrayList(roundToEdges.keySet());
                Collections.sort(sortedRounds);
                List<DagVertexReference> updatedWeakEdges = new ArrayList<>();
                for (Integer round : sortedRounds) {
                    List<DagVertexReference> atRound = roundToEdges.get(round);
                    Collections.shuffle(atRound, new SecureRandom());
                    if (updatedWeakEdges.size() + atRound.size() <= byzantineThresholdF) {
                        updatedWeakEdges.addAll(atRound);
                    } else {
                        atRound = atRound.stream().limit(byzantineThresholdF - updatedWeakEdges.size()).collect(Collectors.toList());
                        updatedWeakEdges.addAll(atRound);
                        break;
                    }
                }
                weak_edges = updatedWeakEdges;
            }
        }
        // returns the weak edges
        return weak_edges;
    }

}
