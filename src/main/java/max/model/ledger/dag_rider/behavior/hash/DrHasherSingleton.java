/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.hash;


import java.util.HashMap;


/**
 * A Singleton that specifies the hasher for transactions and vertices of the DAG in a given DagRider environment
 *
 * @author Erwan Mahe
 */
public final class DrHasherSingleton {


    // The field must be declared volatile
    private static volatile DrHasherSingleton instance;

    private final HashMap<String,DrAbstractHasherForParentSecuring<?>> concrete_hashers;

    private DrHasherSingleton() {
        this.concrete_hashers = new HashMap<>();
    }

    public static DrHasherSingleton getInstance() {
        // Double-checked locking (DCL) prevents race condition between multiple
        // threads that may attempt to get singleton instance at the same time,
        // creating separate instances as a result.
        //
        // The `result` variable is important here.
        //
        // https://refactoring.guru/fr/design-patterns/singleton
        // https://refactoring.guru/java-dcl-issue
        DrHasherSingleton result = instance;
        if (result != null) {
            return result;
        }
        synchronized(DrHasherSingleton.class) {
            if (instance == null) {
                instance = new DrHasherSingleton();
            }
            return instance;
        }
    }

    public static void resetHasher() {
        instance = null;
    }

    public DrAbstractHasherForParentSecuring<?> get_concrete_hasher_for_DR_environment(String env_name) {
        return this.concrete_hashers.get(env_name);
    }

    public void initialize_concrete_hasher_for_DR_environment(String env_name, DrAbstractHasherForParentSecuring<?> hasher) {
        if (this.concrete_hashers.containsKey(env_name)) {
            throw new RuntimeException("initialized twice a concrete hasher for the same DagRider environment of name : " + env_name);
        }
        this.concrete_hashers.put(env_name, hasher);
    }

}