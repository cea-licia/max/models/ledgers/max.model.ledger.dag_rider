/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.proposer;

/**
 * How Weak edges should be selected :
 * If there are more than F possible weak edges one may:
 * - retain all of them
 * - only keep the F oldest
 * - ...
 *
 * @author Erwan Mahe
 * **/
public enum WeakEdgesSelectionPolicy {
    Retain_F_Oldest,
    Retain_F_Random,
    Retain_All
}
