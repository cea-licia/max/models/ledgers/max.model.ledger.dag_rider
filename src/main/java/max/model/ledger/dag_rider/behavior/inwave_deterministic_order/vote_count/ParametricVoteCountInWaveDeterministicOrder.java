/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.behavior.inwave_deterministic_order.vote_count;


import max.model.ledger.dag_rider.behavior.graph.DagRiderGraph;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.graph.DagVertexCoordinates;
import max.model.ledger.dag_rider.behavior.inwave_deterministic_order.AbstractInWaveDeterministicOrder;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.*;


/**
 * The deterministic order between vertices of different nodes at each round must be set
 * it must be the same for every node
 * We implement it as follows :
 * - we compute a vote table that gives the round at which each node "votes" for each vertex of the wave
 * - we define an order of vertices : vertex "x" before "x'" iff:
 *   "the number of nodes that vote x first" GREATERTHAN "the number of nodes that vote x' first" + "a certain threshold"
 * - we then build a directed graph that represents this partial order
 * - we then identify all "sink strongly connected component" in that graph
 * - add the vertices of the SSCCs, sorted by hashcode values, to the list of vertices to deliver and remove them from the graph
 * - and then repeat until the graph is emptied
 * - finally return the list of vertices to deliver
 *
 * @author Erwan Mahe
 * **/
public class ParametricVoteCountInWaveDeterministicOrder<T_tx> implements AbstractInWaveDeterministicOrder<T_tx> {

    /**
     * The threshold on the difference in the number of earlier votes
     * used to determine if a vertex should come earlier than another
     * **/
    int threshold;


    public ParametricVoteCountInWaveDeterministicOrder(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public ArrayList<DagVertex<T_tx>> order_vertices(
            DagRiderGraph<T_tx> graph,
            HashMap<Integer, List<DagVertex<T_tx>>> verticesToDeliverRankedByRound,
            int wave_number
    ) {

        HashMap<DagVertexCoordinates, HashMap<String,Integer>> verticesBoard = VoteCountUtilities.compute_vote_table(
                graph,
                verticesToDeliverRankedByRound
        );

        var comparator = VoteCountUtilities.get_vertex_comparator_from_vote_table(
                graph,
                verticesBoard,
                this.threshold
        );

        DefaultDirectedGraph<DagVertex<T_tx>, DefaultEdge> relation_as_graph = VoteCountUtilities.get_relation_as_graph_from_comparator(
                verticesToDeliverRankedByRound,
                comparator
        );

        // in the directed graph, there may be cycles due to the Condorcet paradox
        // to deal with that, we do not use the partial order to sort the vertices directly
        // we rather identify the Strongly Connected Components of the graphs, each corresponding to a set of vertices that may be ordered in any order
        // within those SCCs there must be one "sink SCC" i.e., an SCC with no ougoing edges (to vertices outside the SCC itself)
        // the vertices of this "sink SCC" must come first
        // once they are added, we remove all vertices of the sink SCC from the graph
        // and repeat the process until the graph is emptied

        // the final list of transactions to deliver, in the order of delivery
        ArrayList<DagVertex<T_tx>> toDeliver = new ArrayList<>();

        // while there are still nodes in the graph
        while (!relation_as_graph.vertexSet().isEmpty()) {
            // in that case the vertices it contains must be ordered
            List<DagVertex<T_tx>> verticesInSinksSCC = new ArrayList<>();
            for (Set<DagVertex<T_tx>> sscc : VoteCountUtilities.get_sink_strongly_connected_components(relation_as_graph)) {
                relation_as_graph.removeAllVertices(sscc);
                verticesInSinksSCC.addAll(sscc);
            }
            // now sort the *verticesInSinksSCC* by hash values
            Collections.sort(verticesInSinksSCC, new Comparator<DagVertex<T_tx>>() {
                @Override
                public int compare(DagVertex<T_tx> o1, DagVertex<T_tx> o2) {
                    return o1.hashCode() - o2.hashCode();
                }
            });
            // and add it to the *toDeliver*
            toDeliver.addAll(verticesInSinksSCC);
        }

        return toDeliver;
    }

}
