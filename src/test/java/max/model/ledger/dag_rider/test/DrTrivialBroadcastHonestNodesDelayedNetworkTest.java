/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.test;


import max.core.action.ACTakeRole;
import max.core.role.RExperimenter;
import max.model.ledger.abstract_ledger.action.check.AcCheckCoherence;
import max.model.ledger.abstract_ledger.action.emission.LedgerTransactionBroadcastFromClientToNodesPolicy;
import max.model.ledger.abstract_ledger.approval.AcceptingTransactionApprovalPolicy;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupLocalLedgerState;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransactionGenerator;
import max.model.ledger.abstract_ledger.usecase.puzzle.action.BuiltinAcRevealNewPuzzle;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.behavior.hash.DrHasherSingleton;
import max.model.ledger.dag_rider.behavior.inwave_deterministic_order.PerColumnShuffleInWaveDeterministicOrder;
import max.model.ledger.dag_rider.behavior.proposer.HonestDagVertexProposer;
import max.model.ledger.dag_rider.behavior.proposer.WeakEdgesSelectionPolicy;
import max.model.ledger.dag_rider.draw.AcDrawDagRiderDags;
import max.model.ledger.dag_rider.exp.DagRiderExperimenter;
import max.model.ledger.dag_rider.exp.ReliableBroadcastLayerImplementation;
import max.model.ledger.dag_rider.role.RDagRiderChannel;
import max.model.ledger.dag_rider.role.RDagRiderNode;
import max.model.ledger.dag_rider.usecase.puzzle.action.AcAddHeartbeatTransactionAndProgressNodes;
import max.model.network.delay_distributions_lib.distros.ContinuousUniformDelay;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.context.delay.FixedDelay;
import max.model.network.stochastic_adversarial_p2p.msgcount.MessageCounterSingleton;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;

/**
 * Simulations of DagRider with only Honest nodes.
 * The reliable broadcast layer is approximated via a trivial broadcast.
 *
 * We have as many clients as there are nodes.
 * Each client sends its transactions to a single node.
 * The network is has no loss but there are random delays between emissions and receptions.
 *
 * @author Erwan Mahe
 */
public class DrTrivialBroadcastHonestNodesDelayedNetworkTest {

    @BeforeEach
    public void before(@TempDir Path tempDir) {
        clearParameters();

        PuzzleMockupTransactionGenerator.resetGenerator();
        DrHasherSingleton.resetHasher();
        MessageCounterSingleton.resetCounter();

        setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
        setParameter("MAX_CORE_UI_MODE", "Silent");
        setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
    }

    @ParameterizedTest
    @CsvSource({"1"})
    public void test(
            int byzantineThreshold,
            TestInfo testInfo)
            throws Throwable {

        int nodesCount = (3*byzantineThreshold) + 1;

        // Test duration in ticks
        final var testDuration = 500;

        // rate of new puzzles
        BigDecimal new_puzzles_rate = BigDecimal.valueOf(10);

        // hearbeat rate
        BigDecimal heartbeat_rate = BigDecimal.valueOf(1);

        // transmission delay from client to Node
        Optional<DelaySpecification> clientsToNodesDelaySpecification = Optional.empty();

        HashMap<
                String,
                Pair<DelaySpecification,LedgerTransactionBroadcastFromClientToNodesPolicy>
                > clientsInformation = new HashMap<>();
        for (int i=0;i<nodesCount;i++) {
            String clientName = "client" + i;
            // ***
            String nodeName = "node" + i;
            HashSet<String> associated_nodes = new HashSet<>();
            associated_nodes.add(nodeName);
            LedgerTransactionBroadcastFromClientToNodesPolicy policy = new LedgerTransactionBroadcastFromClientToNodesPolicy(
                Optional.of(associated_nodes),
                    Optional.empty()
            );
            // ***
            clientsInformation.put(clientName, Pair.of(new FixedDelay(0),policy));
        }

        Optional<DelaySpecification> node_to_node_output_delay = Optional.of(new ContinuousUniformDelay(1,20));
        Optional<DelaySpecification> node_to_node_input_delay = Optional.of(new ContinuousUniformDelay(1,20));

        // Setup tester
        final var tester =
                new DagRiderExperimenter(ReliableBroadcastLayerImplementation.TRIVIAL) {

                    @Override
                    protected void setupDrNodes() {
                        for (var i = 0;
                             i < nodesCount;
                             ++i) {
                            final var plan = this.build_dag_rider_node_plan(
                                    new AcceptingTransactionApprovalPolicy<>(),
                                    node_to_node_output_delay,
                                    node_to_node_input_delay,
                                    new HonestDagVertexProposer<>(false, WeakEdgesSelectionPolicy.Retain_All),
                                    new PerColumnShuffleInWaveDeterministicOrder<>(),
                                    byzantineThreshold
                            );
                            DagRiderAgent agent = new DagRiderAgent(plan);
                            agent.setName("node" + i);
                            this.addDrNode(agent);
                        }
                    }

                    @Override
                    protected void setupExperimenter() {
                        final var drEnvironmentName = this.drEnvironment.getName();
                        schedule(
                                new ACTakeRole<>(drEnvironmentName, RExperimenter.class, this.drEnvironment).oneTime(BigDecimal.ZERO));
                        schedule(
                                new ACTakeRole<>(drEnvironmentName, RDagRiderChannel.class, this.drEnvironment).oneTime(BigDecimal.ZERO));

                        Function<PuzzleMockupTransaction,String> transactionPrinter = pmt -> {
                            return pmt.clientName.get().substring(6) + ":" + pmt.puzzleIdentifier;
                        };

                        boolean drawDAGs = false;
                        if (drawDAGs) {
                            String pathPrefix = "C:\\Users\\em244186\\idea_projects\\dag_rider\\";

                            schedule(
                                    new AcDrawDagRiderDags(drEnvironmentName,
                                            this.drEnvironment,
                                            transactionPrinter,
                                            pathPrefix,
                                            new HashMap<>())
                                            .oneTime(testDuration));
                        }

                        schedule(
                                new BuiltinAcRevealNewPuzzle(
                                        drEnvironmentName,
                                        this.drEnvironment,
                                        RDagRiderNode.class,
                                        clientsToNodesDelaySpecification,
                                        clientsInformation,
                                        Optional.empty())
                                        .repeatFinitely(BigDecimal.valueOf(3),BigDecimal.valueOf(testDuration - 3),new_puzzles_rate)
                        );

                        schedule(
                                new AcAddHeartbeatTransactionAndProgressNodes(
                                        drEnvironmentName,
                                        this.drEnvironment,
                                        Optional.empty(),
                                        false)
                                        .repeatFinitely(BigDecimal.valueOf(3),BigDecimal.valueOf(testDuration - 3),heartbeat_rate)
                        );

                        schedule(
                                new AcCheckCoherence<PuzzleMockupTransaction, PuzzleMockupLocalLedgerState>(
                                        drEnvironmentName,
                                        this.drEnvironment,
                                        RDagRiderNode.class).oneTime(testDuration)
                        );

                    }
                };
        launchTester(tester, testDuration, testInfo);
    }

}
