/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.ledger.dag_rider.exp;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;


import max.model.broadcast.abstract_reliable_broadcast.env.AbstractBroadcastEnvironment;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;
import max.model.broadcast.bracha_brb.action.AcSetPeerBroadcastApprovalPolicy;
import max.model.broadcast.bracha_brb.action.AcSetPeerBroadcastByzantineThreshold;
import max.model.broadcast.bracha_brb.action.AcSetPeerBroadcastLocalState;
import max.model.broadcast.bracha_brb.env.BrachaBrbEnvironment;
import max.model.broadcast.bracha_brb.state.DefaultLocalBrbNodeState;
import max.model.broadcast.trivial_broadcast.env.TrivialBroadcastEnvironment;
import max.model.ledger.abstract_ledger.approval.AbstractTransactionApprovalPolicy;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupLocalLedgerState;
import max.model.ledger.abstract_ledger.usecase.puzzle.PuzzleMockupTransaction;
import max.model.ledger.dag_rider.action.config.AcSetupDagRiderNode;
import max.model.ledger.dag_rider.action.protocol.AcInitializeDAG;
import max.model.ledger.dag_rider.agent.DagRiderAgent;
import max.model.ledger.dag_rider.behavior.graph.DagVertex;
import max.model.ledger.dag_rider.behavior.hash.DrHasherSingleton;
import max.model.ledger.dag_rider.behavior.inwave_deterministic_order.AbstractInWaveDeterministicOrder;
import max.model.ledger.dag_rider.behavior.proposer.DagRiderVertexProposer;
import max.model.ledger.dag_rider.env.DagRiderEnvironment;
import max.model.ledger.dag_rider.role.RDagRiderNode;
import max.model.ledger.dag_rider.usecase.puzzle.mockup.MockupTransactionHasher;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import one.util.streamex.StreamEx;


/**
 * Base experimenter for our tests.
 *
 * @author Erwan Mahe
 */
public abstract class DagRiderExperimenter extends ExperimenterAgent {


    /** DagRider environment. */
    protected DagRiderEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState> drEnvironment;

    protected AbstractBroadcastEnvironment<DagVertex<PuzzleMockupTransaction>,DagRiderAgent> bEnvironment;


    /** DagRider nodes. */
    private final Map<String, DagRiderAgent> drNodes;

    private final ReliableBroadcastLayerImplementation reliableBroadcastLayerImplementation;

    /**
     * Build a new experimenter instance.
     */
    public DagRiderExperimenter(ReliableBroadcastLayerImplementation reliableBroadcastLayerImplementation) {
        this.drNodes = new HashMap<>();
        this.reliableBroadcastLayerImplementation = reliableBroadcastLayerImplementation;
    }

    /**
     * Sets up the environment.
     */
    protected void setupEnvironment() {
        // sets up the dagrider environment
        this.drEnvironment = new DagRiderEnvironment<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>();
        // sets up the hasher for the transactions and vertices
        DrHasherSingleton.getInstance().initialize_concrete_hasher_for_DR_environment(this.drEnvironment.getName(),new MockupTransactionHasher());
        // sets up the broadcast environment
        if (this.reliableBroadcastLayerImplementation.equals(ReliableBroadcastLayerImplementation.TRIVIAL)) {
            this.bEnvironment = new TrivialBroadcastEnvironment<DagVertex<PuzzleMockupTransaction>, DagRiderAgent>(this.drEnvironment.getName());
        } else if (this.reliableBroadcastLayerImplementation.equals(ReliableBroadcastLayerImplementation.BRACHA)) {
            this.bEnvironment = new BrachaBrbEnvironment<>(this.drEnvironment.getName());
        }
        // notifies the dagrider environment of the name of the broadcast environment
        this.drEnvironment.reliableBroadcastEnvironmentName = this.bEnvironment.getName();
    }

    /**
     * Adds a DagRider node to the simulation.
     *
     * @param node node instance to add
     */
    protected void addDrNode(DagRiderAgent node) {
        drNodes.put(node.getName(), node);
    }


    public List<String> getDrNodesNames() {
        return this.drNodes.keySet().stream().sorted().collect(Collectors.toList());
    }

    /**
     * Sets up the DagRider nodes in the simulation.
     * Implementation details are left to the concrete simulation testcase.
     */
    protected abstract void setupDrNodes();

    /**
     * Scenario contains 1 agent by default corresponding to the DagRider environment
     *
     * @return unmodifiable list of the agents.
     */
    @Override
    protected List<MAXAgent> setupScenario() {
        this.setupEnvironment();
        this.setupDrNodes();

        Stream<MAXAgent> nodes = this.drNodes.values().stream().map(x -> (MAXAgent) x);

        Stream<MAXAgent> resultingStream = StreamEx.of(Stream.of( (MAXAgent) this.drEnvironment))
                .append(Stream.of( (MAXAgent) this.bEnvironment))
                .append(nodes);

        return resultingStream.filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Build a plan for DagRider Nodes
     */
    protected Plan<DagRiderAgent> build_dag_rider_node_plan(
            AbstractTransactionApprovalPolicy<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState> approvalPolicy,
            Optional<DelaySpecification> node_to_node_output_delay,
            Optional<DelaySpecification> node_to_node_input_delay,
            DagRiderVertexProposer<PuzzleMockupTransaction> vertexProposer,
            AbstractInWaveDeterministicOrder<PuzzleMockupTransaction> deterministicOrder,
            Integer byzantineThreshold
    ) {
        // Static part
        final List<ActionActivator<DagRiderAgent>> static_plan = new ArrayList<>();
        final var join_commitee_time = getCurrentTick();
        final var setup_time = join_commitee_time.add(BigDecimal.valueOf(1));
        final var genesis_time = setup_time.add(BigDecimal.valueOf(1));

        // Have the node join the DagRider commitee
        static_plan.add(new ACTakeRole<DagRiderAgent>(this.drEnvironment.getName(), RDagRiderNode.class,null).oneTime(join_commitee_time));
        static_plan.add(new ACTakeRole<DagRiderAgent>(this.bEnvironment.getName(), RBroadcastPeer.class,null).oneTime(join_commitee_time));

        // Node setup configuration
        static_plan.add(
                new AcSetupDagRiderNode<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>(
                        this.drEnvironment.getName(),
                        null,
                        new PuzzleMockupLocalLedgerState(),
                        approvalPolicy,
                        node_to_node_output_delay,
                        node_to_node_input_delay,
                        vertexProposer,
                        deterministicOrder
                ).oneTime(setup_time)
        );

        if (this.reliableBroadcastLayerImplementation.equals(ReliableBroadcastLayerImplementation.BRACHA)) {
            static_plan.add(
                    new AcSetPeerBroadcastApprovalPolicy<DagVertex<PuzzleMockupTransaction>,DagRiderAgent,DefaultLocalBrbNodeState<DagVertex<PuzzleMockupTransaction>>>(
                            this.bEnvironment.getName(),
                            null,
                            x -> true
                    ).oneTime(setup_time)
            );

            static_plan.add(
                    new AcSetPeerBroadcastByzantineThreshold<DagVertex<PuzzleMockupTransaction>,DagRiderAgent,DefaultLocalBrbNodeState<DagVertex<PuzzleMockupTransaction>>>(
                            this.bEnvironment.getName(),
                            null,
                            byzantineThreshold
                    ).oneTime(setup_time)
            );

            static_plan.add(
                    new AcSetPeerBroadcastLocalState<DagVertex<PuzzleMockupTransaction>,DagRiderAgent,DefaultLocalBrbNodeState<DagVertex<PuzzleMockupTransaction>>>(
                            this.bEnvironment.getName(),
                            null,
                            new DefaultLocalBrbNodeState<>()
                    ).oneTime(setup_time)
            );
        }

        // Initialize the DAG at the start of the simulation (genesis)
        static_plan.add(new AcInitializeDAG<PuzzleMockupTransaction,PuzzleMockupLocalLedgerState>(this.drEnvironment.getName(),null).oneTime(genesis_time));

        return new Plan<>() {
            @Override
            public List<ActionActivator<DagRiderAgent>> getInitialPlan() {
                return static_plan;
            }
        };

    }




}